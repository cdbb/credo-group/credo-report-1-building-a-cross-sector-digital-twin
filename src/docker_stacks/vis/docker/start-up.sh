#!/bin/sh

# This script should run whenever the Docker container is started. It is expected
# to be located, and executed from, the /var/www/html/dafni directory.

echo "Running start-up.sh script..."
echo "Running as $(whoami)"

# Update the backends.json file
BACKENDS_FILE="./backends.json"
rm -f /p/a/t/h ${BACKENDS_FILE}

echo "{" >> ${BACKENDS_FILE}
echo "\"visualisationApiUrl\":\"${VISUALISATION_API_URL}\"," >> ${BACKENDS_FILE}
echo "\"nivsMinioUrl\":\"${NIVS_MINIO_URL}\"," >> ${BACKENDS_FILE}
echo "\"nidMinioUrl\":\"${NID_MINIO_URL}\"," >> ${BACKENDS_FILE}
echo "\"nidApiUrl\":\"${NID_URL}\"," >> ${BACKENDS_FILE}
echo "\"dssauthUrl\":\"${DSS_AUTH}\"," >> ${BACKENDS_FILE}
echo "\"node_env\":\"${NODE_ENV}\"," >> ${BACKENDS_FILE}
echo "\"instanceID\":\"${INSTANCE_ID}\"" >> ${BACKENDS_FILE}
echo "}" >> ${BACKENDS_FILE}
echo "The ./backends.json file has now been populated."

# Update the keycloak.json file
KEYCLOAK_FILE="./keycloak.json"
rm -f /p/a/t/h ${KEYCLOAK_FILE}

echo "{" >> ${KEYCLOAK_FILE}
echo "\"keycloakUrl\":\"${KEYCLOAK_ENDPOINT}\"," >> ${KEYCLOAK_FILE}
echo "\"keycloakRealm\":\"${KEYCLOAK_REALM}\"," >> ${KEYCLOAK_FILE}
echo "\"keycloakClientId\":\"${KEYCLOAK_CLIENT}\"," >> ${KEYCLOAK_FILE}
echo "\"cookieName\":\"${KEYCLOAK_LOGIN_COOKIE_NAME}\"," >> ${KEYCLOAK_FILE}
echo "\"cookieDomain\":\"${KEYCLOAK_LOGIN_COOKIE_DOMAIN}\"," >> ${KEYCLOAK_FILE}
echo "\"cookieExpirationHours\":2," >> ${KEYCLOAK_FILE}
echo "\"redirectDomainCheck\":\"${KEYCLOAK_LOGIN_REDIRECT_CHECK}\"" >> ${KEYCLOAK_FILE}
echo "}" >> ${KEYCLOAK_FILE}
echo "The ./keycloak.json file has now been populated."

# Replace the "VISUUID" string in the conf file with the contents of the INSTANCE_ID
# environment variable. DAFNI requires content to be accessed on the "/instance/uuid" URL.
sed "s/VISUUID/${INSTANCE_ID}/g" /etc/apache2/custom-settings.conf | sponge /etc/apache2/custom-settings.conf

echo "Added alias to support DAFNI URL pattern, accessible on:"
echo "    localhost:5001/instance/${INSTANCE_ID}"

# Start the Apache web server
echo "Starting the Apache web server..."
apache2ctl start

# Start tomcat (to host geoserver)
echo "Starting the Tomcat (hosting Geoserver) server..."
catalina.sh run &
/usr/local/bin/geoserver-rest-config.sh &

# Start the NodeJS web server
echo "Starting NodeJS web server..."
npm start