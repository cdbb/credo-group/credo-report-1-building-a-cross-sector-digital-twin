/**
 * Contacts the Node server to ask if data has already been downloaded.
 */
async function checkForData() {
    // Determine base URL
    var loc = window.location;
    var baseURL = loc.protocol + "//" + loc.host + "/" + loc.pathname;
    if (baseURL.endsWith("/")) baseURL = baseURL.substring(0, baseURL.length - 1);

    let tempURL = new URL(baseURL);
    let checkURL = tempURL.toString() + ((tempURL.toString().endsWith("/")) ? "check" : "/check");

    const response = await axios({
        url: checkURL,
        method: "get",
        timeout: 900_000,
        headers: {
            "Content-Type": "text/plain",
        }
    }).catch(function (error) {
        if (error.response) {
            console.log(error.response);
            console.log(error.response.status);
            console.log(error.response.data);

            let result = [error.response.status, error.response.data];
            return Promise.reject(result);
        }
    });
    return [response.status, response.body];
}

/**
 * Loop for up to 30 seconds until the server reports that the data
 * has all been downloaded to the file system.
 * 
 * @param {Function} callback callback function.
 */
function loopCheckForData(callback) {
    var done = false;

    // Check every 3 seconds for up to 5 minutes
    for (var i = 0; i < 100; i++) {
        sleep(i * 3000).then(() => {
            if (!done) {

                checkForData()
                    .then(function (result) {
                        if (result[0] != 204) {
                            done = true;
                            if (callback != null) callback(result);
                        }
                    })
                    .catch(function (error) {
                        done = true;
                        if (callback != null) callback(error);
                    });
            }
        });
    }
}

/**
 * Start the dataset download process.
 * 
 * @param {Function} callback callback to fire when data downloaded.
 */
function download(token, callback) {
    readBackendJSON().then(settings => {
        downloadData(settings, token, callback);
    });
}

/**
 * Determine data URLs and download.
 * 
 * @param {Object} settings backend settings.
 * @param {String} token keycloak token.
 * @param {Function} callback to fire after download.
 */
function downloadData(settings, token, callback) {
    // Get the instance ID
    let instance = settings.instanceID;
    let visAPI = settings.visualisationApiUrl;

    console.log("Getting visualisation instance info...");
    getInstance(visAPI, instance, token).then(visData => {
        if (visData === null) {
            console.error("Could not contact visualisation API URL.");
            return;
        }

        // Get IDs of datasets
        console.log("Getting data set IDs...");
        let datasetIDs = getDatasetIDs(visData);

        // Get the URLs for those datasets
        console.log("Getting URLs for files...");
        getDatasetURLs(settings.nidApiUrl, token, datasetIDs).then(nidData => {
            if (nidData === null) {
                console.error("Could not contact visualisation API URL.");
                return;
            }

            // Generate request data to send to server side
            let requestData = generateServerData(settings.nidMinioUrl, token, nidData);

            // Send details to server so that it can download and write files to disk
            sendToServer(requestData).then(response => {
                if (response === null || response.status !== 200) {
                    allSuccess = false;
                }

                if (callback != null) callback();
            });
        });
    });
}

/**
 * Sends a request to the NodeJS server within this container
 * to handle file downloads on the server-side.
 * 
 * @param {Object} requestData JSON data about files.
 * 
 * @returns request response.
 */
async function sendToServer(requestData) {
    console.log("Sending list of files to download to NodeJS...");

    var loc = window.location;
    var baseURL = loc.protocol + "//" + loc.host + "/" + loc.pathname.split('/')[1];
    if (baseURL.endsWith("/")) baseURL = baseURL.substring(0, baseURL.length - 1);

    let tempURL = new URL(window.location);
    let writeURL = tempURL.toString() + ((tempURL.toString().endsWith("/")) ? "write" : "/write");

    let serverResponse = await axios({
        url: writeURL,
        method: "post",
        timeout: 900_000,
        data: requestData
    });

    if (serverResponse.status === 200) {
        return serverResponse.data;
    } else {
        return null;
    }
}

/**
 * Generates JSON data about what files to download so that it
 * can be send to NodeJS and handled server-side.
 * 
 * @param {String} nidMinioUrl core download URL.
 * @param {String} token keycloak token.
 * @param {Object[]} datasets list of datasets.
 * 
 * @returns built JSON data for request.
 */
function generateServerData(nidMinioUrl, token, datasets) {
    let requestData = {};
    requestData.token = token;
    requestData.files = [];

    datasets.forEach(dataset => {
        let files = dataset.files_with_urls;

        files.forEach(file => {
            let entry = {};

            entry.url = replaceMinioURL(file.url, nidMinioUrl);
            entry.name = file.file;
            requestData.files.push(entry);
        });
    });
    return requestData;
}

/**
 * Determine the URLs for each dataset.
 * 
 * @param {String} nidAPI NID api url.
 * @param {String} token keycloak token.
 * @param {String[]} ids dataset ids.
 * 
 * @returns URLs for datasets.
 */
async function getDatasetURLs(nidAPI, token, ids) {
    let nidURL = nidAPI + "/version/batch/";
    let auth = "Bearer " + token;

    let nidResponse = await axios({
        url: nidURL,
        method: "post",
        timeout: 90_000,
        headers: {
            "Authorization": auth
        },
        data: {
            "version_uuids": ids
        }
    });

    if (nidResponse.status === 200) {
        console.log("Got information on the dataset URLs.");
        return nidResponse.data;
    } else {
        return null;
    }
}


/**
 * Update the Minio URL.
 * 
 * @param {String} originalURL original URL.
 * @param {String} replacementURL replacement URL.
 * 
 * @returns updated URL.
 */
function replaceMinioURL(originalURL, replacementURL) {
    if (!replacementURL) return originalURL;

    const dafniUrl = 'dafni.rl.ac.uk';
    const dafniIndex = originalURL.indexOf(dafniUrl);
    const indexToSlice = dafniIndex + dafniUrl.length;
    const originalURLMinusHost = originalURL.slice(indexToSlice);

    return replacementURL.concat(originalURLMinusHost);
}

/**
 * Get the data entry for the visualisation with the input instance ID.
 * 
 * @param {String} visAPI visualisation API.
 * @param {String} instance visualisation instance ID.
 * @param {String} token keycloak token.
 * 
 * @returns visualisation instance data.
 */
async function getInstance(visAPI, instance, token) {
    let instanceURL = visAPI + "/instances/" + instance;
    let auth = "Bearer " + token;

    let instanceResponse = await axios({
        url: instanceURL,
        method: "get",
        timeout: 30_000,
        headers: {
            "Authorization": auth
        }
    });

    if (instanceResponse.status === 200) {
        console.log("Got information on the visualisation instance.");
        return instanceResponse.data;
    } else {
        return null;
    }
}

/**
 * Determine the IDs for the datasets linked to the
 * input visualisation definition.
 * 
 * @param {Object} visData visualisation data.
 *  
 * @returns dataset IDs.
 */
function getDatasetIDs(visData) {
    let ids = [];
    let assets = visData.visualisation_assets;

    assets.forEach(asset => {
        let type = asset.asset_type;
        if (type === "dataset") {
            ids.push(asset.asset_id);
        }
    });
    return ids;
}

/**
 * Reads the backend JSON file.
 * 
 * @returns JSONObject containing backend settings.
 */
async function readBackendJSON() {
    try {
        const response = await axios({
            url: "./dafni/backends.json",
            method: "get",
            timeout: 30_000,
            headers: {
                "Content-Type": "application/json",
            }
        });

        if (response.status === 200) {
            console.log("Backend JSON configuration read successfully.");
            return response.data;
        }
    } catch (error) {
        console.error("ERROR: Can't access backend JSON configuration!");
    }
    return null;
}

/**
 * Sleep.
 * 
 * @param {Integer} ms milliseconds 
 * 
 * @returns promise
 */
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}