const express = require("express");
const parser = require("body-parser");
const axios = require("axios");
const path = require("path");
const cors = require('cors');
const fs = require("fs");
const util = require('util');
const execSync = require('child_process').execSync;

// Ports
const port = 5002;

// Create an Express app to server data
const app = express();
app.use(express.static(__dirname));
app.use(cors());
app.use(parser.json({ limit: "5gb" }));
app.use(parser.urlencoded({ extended: true }));

// Is all data downloaded?
var dataComplete = false;
var errorOccurred = false;
var downloadStarted = false;

// Size of all data downloaded
var totalLength = 0;

// Log to terminal and file
var log_file = fs.createWriteStream(__dirname + '/server.log', { flags: 'w' });
var log_stdout = process.stdout;
console.log = function (d, fileOnly = false) {
    if (!fileOnly) log_stdout.write(util.format(d) + '\n');
    log_file.write(util.format(d) + '\n');
};

/**
 * Start server and wait for connections
 */
function startup() {
    // File writing route. Expects JSON in request, will use
    // to download and write data files to disk.
    app.post("/write", function (request, response) {

        if (downloadStarted) {
            console.log("File download has already started, skipping this request...");
            response.status(204);
            response.send("WARNING: Data files not yet downloaded.");
            return;
        }

        console.log("Received a file write request, will attempt file download...");

        var start = new Date();
        console.log("Starting download process at " + formatDate(start));

        // Prepare the target directory
        prepareDirectory();

        // Download files
        errorOccurred = false;
        downloadStarted = true;

        //let promises = downloadFiles(request.body.token, request.body.files);
        downloadFiles(request.body.token, request.body.files).then(() => {
            if (errorOccurred) {
                console.log("ERROR: Could not download all files.");
            } else {
                var duration = (new Date()).getTime() - start.getTime();
                var totalSize = (totalLength / 1024 / 1024).toFixed(2);

                console.log("Duration was " + (duration / 1000) + " seconds.");
                console.log("Total size estimate of all downloads is " + totalSize + "MB");
                console.log("All files have been downloaded.");
            }

            // Start the Geoserver
            console.log("Now starting Geoserver...");
            try {
                let script_stdout = execSync("/usr/local/bin/geoserver-rest-config.sh");

                if (script_stdout.includes("Finished configuring GeoServer")) {
                    console.log("Geoserver has reported it is ready!");
                    dataComplete = true;
                } else {
                    console.error("Error has occurred whilst configuring GeoServer.\n" + script_stdout);
                    dataComplete = true;
                    errorOccurred = true;
                }
            } catch (e) {
                console.error("Error has occurred whilst configuring GeoServer.\n" + e);
                dataComplete = true;
                errorOccurred = true;
            }

            downloadStarted = false;
        });

        // Send response now
        response.status(200);
        response.send("Data download started.");
    });

    // Check if the data files are already downloaded
    app.get("/check", function (request, response) {

        if (!downloadStarted) {
            let dataCheck = checkData();
            if (dataCheck.length > 0) {
                response.status(204);
                response.send("WARNING: Data files not yet downloaded.");
            } else {
                response.status(200);
                response.send("SUCCESS: Data files already present.");
            }
            return;
        }

        if (dataComplete) {
            if (!errorOccurred) {
                // Perform a quick validity check on the data
                let dataCheck = checkData();
                if (dataCheck.length > 0) {
                    response.status(422);
                    response.send(dataCheck);

                    errorOccurred = true;
                    return;
                }
            }
            if (errorOccurred) {
                response.status(500);
                response.send("ERROR: Have tried to download data, but failed.");
            } else {
                response.status(200);
                response.send("SUCCESS: Data files already present.");
            }

        } else {
            response.status(204);
            response.send("WARNING: Data files not yet downloaded.");
        }
    });

    // Health check required by DAFNI
    app.get("/healthz", function (request, response) {
        response.status(200);
        response.send("SUCCESS: Container is running.");
    });

    // Start the server on the chosen port
    app.listen(port, function () {
        console.log(`Server is now listening on port ${port}...`);
    });
}

/**
 * Download data files to the local file system.
 * 
 * @param {String} token keycloak token.
 * @param {Object} files files for download.
 */
async function downloadFiles(token, files) {
    for (var i = 0; i < files.length; i += 5) {
        let chunk = files.slice(i, i + 5);
        let chunkPromises = [];

        chunk.forEach(file => {
            chunkPromises.push(downloadFile(token, file.url, file.name));
        });

        await Promise.all(chunkPromises).then(() => {
            var count = ((i + 5) > files.length) ? files.length : (i + 5);
            var message = "Downloaded " + count + "/" + files.length;
            console.log(message);
        });
    }
}

/**
 * Download a single data file.
 * 
 * @param {String} token keycloak token.
 * @param {String} minioURL file URL.
 * @param {String} name file name.
 * 
 * @returns response data
 */
async function downloadFile(token, minioURL, name) {
    // Download the file data
    let minioResponse = await axios({
        url: minioURL,
        method: "get",
        timeout: 180_000,
        responseType: 'stream',
        headers: {
            "Authorization": ("Bearer " + token)
        }
    }).catch(function (error) {
        console.log("ERROR: Could not download file: " + name);
        console.log(error, true);
        errorOccurred = true;
    });

    if (minioResponse.status === 200) {
        return writeFile(name, minioResponse);
    }
}

/**
 * Write the input object to a file on the local FS.
 * 
 * @param {String} fileName name of file. 
 * @param {Object} response file data.
 */
function writeFile(fileName, response) {
    let target = path.join(process.env.ROOT_DIR, fileName);
    let targetDir = path.join(target, "..");
    if (!fs.existsSync(targetDir)) {
        fs.mkdirSync(targetDir, { recursive: true });
    }

    return new Promise((resolve, reject) => {
        // Write file data to disk
        let writer = fs.createWriteStream(target);
        response.data.pipe(writer);

        response.data.on("error", (error) => {
            console.log("Error writing file: " + target);
            errorOccurred = true;
            reject(error);
        });

        writer.on("close", () => {
            console.log("Written file: " + fileName);
            totalLength += fs.statSync(target).size;
            resolve(true);
        });
    });
}

/**
 * Check the the target directory exists, if it doesn't then create it.
 */
function prepareDirectory() {
    let target = process.env.ROOT_DIR;

    if (fs.existsSync(target)) {
        console.log("Target directory already exists (" + target + ").");
    } else {
        console.log("Target directory not found (" + target + ").");
        fs.mkdirSync(target);
        console.log("Target directory created.");
    }
}

/**
 * Performs a VERY basic check to see if the data folder contains files
 * in the expected format.
 */
function checkData() {
    console.log("Checking data...");

    let response = "";

    let dataDir = path.join(process.env.ROOT_DIR, "data");
    if (!fs.existsSync(dataDir)) {
        response += "Expected there to be a 'data' directory in the root of the 'html' directory.<br/>";
        return response;
    }

    let targetMeta = path.join(dataDir, "meta.json");
    let targetTree = path.join(dataDir, "tree.json");

    if (!fs.existsSync(targetMeta)) {
        response += "Expected there to be a 'meta.json' file in the root of the data directory.<br/>";
    }
    if (!fs.existsSync(targetTree)) {
        response += "Expected there to be a 'tree.json' file in the root of the data directory.<br/>";
    }

    let hasDirectories = false;

    let files = fs.readdirSync(dataDir);
    files.forEach(file => {
        let target = path.join(dataDir, file);

        if (!hasDirectories && fs.statSync(target).isDirectory()) {
            hasDirectories = true;
        }
    });

    if (!hasDirectories) {
        response += "Expected there to be at least one sub-directory within the data directory.<br/>";
    }

    return response;
}

/**
 * 
 * @param {*} date 
 * @returns 
 */
function formatDate(date) {
    return pad(date.getDate(), 2) + "/" +
        pad(date.getMonth() + 1, 2) + "/" +
        date.getFullYear() + " " +
        pad(date.getHours(), 2) + ":" +
        pad(date.getMinutes(), 2) + ":" +
        pad(date.getSeconds(), 2);
}

/**
 * 
 * @param {#} n 
 * @param {*} width 
 * @param {*} z 
 * @returns 
 */
function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

/**
 * Sleep.
 * 
 * @param {Integer} ms milliseconds 
 * 
 * @returns promise
 */
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * ENTRY POINT
 */
startup();