#!/bin/bash

docker stop integrationtest
docker container rm integrationtest
docker system prune -f
docker build -t integrationtest:latest --build-arg GEOSERVER_VERSION=2.19.2 -f docker/Dockerfile .
docker run -it -p 5001-5003:5001-5003 --name "integrationtest" --env INSTANCE_ID='32f7877a-9000-4967-8b13-da55c100910d' --env NID_MINIO_URL="https://fwd.secure.dafni.rl.ac.uk/nidminio" --env NIVS_MINIO_URL="https://fwd.secure.dafni.rl.ac.uk/nivsstore" --env VISUALISATION_API_URL="https://dafni-nivs-api.secure.dafni.rl.ac.uk" --env NID_URL="https://dafni-nid-api.secure.dafni.rl.ac.uk/nid" --env DSS_AUTH="https://dafni-dss-dssauth.secure.dafni.rl.ac.uk" --env NODE_ENV="production" --env KEYCLOAK_ENDPOINT="https://keycloak.secure.dafni.rl.ac.uk/auth/" --env KEYCLOAK_REALM="Production" --env KEYCLOAK_CLIENT="dafni-main" --env KEYCLOAK_CLIENT_ALT="dafni" --env KEYCLOAK_LOGIN_COOKIE_NAME="__Secure-dafni" --env KEYCLOAK_LOGIN_COOKIE_DOMAIN=".secure.dafni.rl.ac.uk" --env KEYCLOAK_LOGIN_REDIRECT_CHECK="dafni.rl.ac.uk" integrationtest:latest