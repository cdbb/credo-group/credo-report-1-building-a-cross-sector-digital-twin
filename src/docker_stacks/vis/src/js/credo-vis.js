/*
*
* This script contains a number functions to add additional functionality to the
* CReDO visualisation, without affecting the centralised visualisation framework.
*
*/

// Handles highlighting selected features.
var _highlighter;

// Handles the focussed "connection" view.
var _focusView;

/**
 * Creates a function callback used to color options in select
 * components when multiple sites are selected.
 */
function setSelectColorer() {
    DT.selectColorer = function(feature) {
        if(feature == null) return "black";

        if(feature["properties"] && feature["properties"]["floodCount"]) {
            let count = feature["properties"]["floodCount"];
            if (count >= 1) return "red";
        }
        return "black";
    };
}

/**
 * Adds visualisation specific functionality that is not generic enough to
 * be provided by the centralised visualisation framework.
 * 
 * @param {DigitalTwinManager} manager current manager object 
 */
function setupAdditionalFunctionality(manager) {
    if(_highlighter == null) {
        _highlighter = new Highlighter(manager.getMap(), manager.getLayerHandler());
    }
    if(_focusView == null) {
        _focusView = new FocusView(manager.getMap(), manager.getRegistry(), _highlighter);
    }

    // Create a callback to fire when features are selected
    let selectionCallback = function(feature) {
        if((feature["geometry"]["type"] === "Point") && (feature["properties"]["icon-image"] != null)) {

            let wasInFocus = DT.focusMode;
            if(wasInFocus) exitFocusMode();

            // Highlight the feature
            _highlighter.highlightFeature(feature);

            // Add additional controls for focus view
            addFeatureControls();
            if(wasInFocus) enterFocusMode();

        } else if(DT.focusMode) {
            addFeatureControls(false);
        }
    }

    // Register selectColorer function
    setSelectColorer();
    
    // Register callback
    manager.addSelectionCallback("*", selectionCallback);
}

/**
 * Pass-through to remove the highlight layer.
 */
 function removeHighlight() {
    if(this._highlighter != null) {
        this._highlighter.remove();
    }
}

/**
 * Toggles the detailed connection view.
 */
function toggleFocusMode() {
    if(DT.focusMode === true) {
        exitFocusMode();
    } else {
        enterFocusMode();
    }
}

/**
 * Enters focus mode and shows depth-1 connections for the currently selected feature.
 */
function enterFocusMode() {
    enableControls(false);
    _highlighter.remove();

    // Add control to change connection depth
    let html = `
        <table id="connectionDepthTable">
            <tr>
                <td width="60%" style="padding-right: 10px;"><p>Maximum Connection Depth:</p></td>    
                <td width="30%"><input id="depthInput" type="number" min="1" max="99" setp="1" value="1"></td>   
                <td width="10%" style="text-align: center;">
                    <div class="tooltip" id="boundsButtonContainer" style="width: 16px; height: 16px;">
                        <img id="boundsButton" src="./img/bounds.png" onclick="snapFocusToBounds()"/>
                        <span class="tooltiptext">Fit map to highlighted features.</span>
                    </div>
                </td>   
            </tr>
            <tr>
                <td colspan="3">
                    <div id="hiddenElementContainer">Non-visible connections and/or sites have been omitted. Please update the Layer selection
                    state before relaunching the Detailed Connection View to see them.</div>
                </td>
            </tr>
        </table>
    `;
    document.getElementById("focusControlsContainer").innerHTML += html;

    // Update the button
    let button = document.getElementById("focusButton");
    button.innerHTML = "Exit Connection View";

    // Trigger logic to show default depth
    this._focusView.showConnectionsForFeature(DT.currentFeature);
    
    // Listen for depth changes
    document.getElementById("depthInput").addEventListener("change", (event) => {
        let depth = event.target.value;
        this._focusView.showConnectionsForFeature(DT.currentFeature, depth);
    });
}

/**
 * Ends the current focus mode.
 */
 function exitFocusMode() {
    // Remove focus view controls
    let button = document.getElementById("focusButton");
    if(button != null) button.innerHTML = "View Direct Connections";

    let container = document.getElementById("focusControlsContainer");
    let depthTable = document.getElementById("connectionDepthTable");
    if(container != null && depthTable != null) container.removeChild(depthTable);

    if(container != null && container.childElementCount === 0) {
        container.style.display = "none";
    }

    _focusView.endFocusMode();
    enableControls(true);

    //Re-highlight the feature
    if((DT.currentFeature["geometry"]["type"] === "Point") && (DT.currentFeature["properties"]["icon-image"] != null)) {
        _highlighter.highlightFeature(DT.currentFeature);
    }
}

/**
 * Snaps the map viewport to contain all connected features.
 */
function snapFocusToBounds() {
    if(DT.focusMode == true) {
        this._focusView.flyToBounds();
    }
}

/**
 * Once a feature is selected, this function generates and adds controls
 * to the side panel allowing the user to enable & configure the focussed
 * connection view.
 */
function addFeatureControls(enter = true) {
    // HTML for button to enter focus view
    let html = `
        <div id="focusControlsContainer">
            <div id="focusButtonContainer">
                <button type="button" id="focusButton" onclick="toggleFocusMode()">View Direct Connections</button>
            </div>
        </div>
    `;

    if(!enter) {
        html = html.replace("View Direct Connections", "Exit Connection View");
    }

    document.getElementById("content-before").innerHTML += html;
    document.getElementById("content-before").style.display = "block";
}

/**
 * Set the enabled state for all Terrain and Layer controls on the left of the map.
 * 
 * @param {Boolean} enable desired state 
 */
function enableControls(enable) {
    let terrainContainer = document.getElementById("terrainContainer");
    let terrainButtons = terrainContainer.querySelectorAll("input[type='radio']");
    terrainButtons.forEach(button => {
        button.disabled = !enable;
    });

    let layerContainer = document.getElementById("layerContainer");
    let layerChecks = layerContainer.querySelectorAll("input[type='checkbox']");
    layerChecks.forEach(check => {
        check.disabled = !enable;
    });

    let selectionsContainer = document.getElementById("selectionsContainer");
    let selects = selectionsContainer.querySelectorAll("select");
    selects.forEach(select => {
        select.disabled = !enable;
    });
}

/**
 * This function obfuscates the location of the map by removing a significant amount of layers
 * and restricting the choice of terrain.
 * 
 * Jethro has asked that this be added so that it can be used when generating video footage
 * of the visualisation for marketing (potentially with dummy data).
 * 
 * @param {MapBox Map} map current map instance
 * @param {Boolean} obfuscate obfuscation state (true, false). 
 */
 function obfuscateLocation(map, obfuscate) {
    // Allowed MapBox layers
    let allowed = ["land", "landcover", "landuse", "hillshade", "waterway", "admin-1-boundary-bg", "admin-0-boundary-bg"];

    // Set layer visibility state
    let layers = map.getStyle().layers;
    for(var i = 0; i < layers.length; i++) {
        if(!allowed.includes(layers[i].id)) {
            map.setLayoutProperty(layers[i].id,	"visibility", (obfuscate) ? "none" : "visible");
        }
    }

    // Show/hide the controls for certain terrain types
    let terrainContainer = document.getElementById("terrainContainer");
    let targetElements = [];

    targetElements = targetElements.concat(
        Array.prototype.slice.call(terrainContainer.querySelectorAll("*[id='blueprint'], *[for='blueprint']"))
    );
    targetElements = targetElements.concat(
        Array.prototype.slice.call(terrainContainer.querySelectorAll("*[id='satellite'], *[for='satellite']"))
    );
    targetElements = targetElements.concat(
        Array.prototype.slice.call(terrainContainer.querySelectorAll("*[id='satellite-streets'], *[for='satellite-streets']"))
    );

    // Udpate visibility for controls
    targetElements.forEach(targetElement => {
        targetElement.style.display = (obfuscate) ? "none" : "block";
    });
}

/**
 * Builds a HTML snippet for a CReDo specific legend.
 */
 function buildLegend() {
    let html = `<div id="credo-legend">`;
    
    html += `<h3>Water Network:</h3>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/aw-clean-water.png"/><p>Clean Water Sites</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/aw-sewage.png"/><p>Sewage Sites</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/aw-sludge.png"/><p>Sludge Sites</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/aw-mast.png"/><p>Masts</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/aw-generic.png"/><p>Other Sites</p></div>`;
    html += `<h3>Communications Network:</h3>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/bt-cabinet.png"/><p>Cabinets</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/bt-exchange.png"/><p>Exchanges</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/bt-mast.png"/><p>Masts</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/bt-generic.png"/><p>Other Sites</p></div>`;
    html += `<h3>Power Network:</h3>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/ukpn-primary.png"/><p>Primary Substations</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/ukpn-secondary.png"/><p>Secondary Substations</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/ukpn-generic.png"/><p>Other Sites</p></div>`;
    html += `<h3>Environment Agency:</h3>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/ea-flow.png"/><p>Flow</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/ea-water-level.png"/><p>Water Level</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/ea-rainfall.png"/><p>Rainfall</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/ea-temperature.png"/><p>Temperature</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/ea-wind.png"/><p>Wind</p></div>`;
    html += `<h3>Buildings:</h3>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/buildings-education.png"/><p>Education</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/buildings-medical.png"/><p>Medical Care</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/buildings-emergency.png"/><p>Emergency Services</p></div>`;
    html += `<div class="credo-legend-entry"><img src="img/asset_icons/buildings-other.png"/><p>Other</p></div>`;

    html += `</div>`;
    return html;
}

/**
 * Finds and updates the layout properties of "_cluster" layers to provide an indication
 * of how many features within the cluster are flooded.
 * 
 * @param {MapBox Map} map current map instance. 
 */
function supportFloodCounts(map) {
    let layers = map.getStyle().layers;

    layers.forEach(layer => {
        let layerName = layer["id"];
        if(layerName.endsWith("_cluster")) {

            // Update the text-field content
            map.setLayoutProperty(layerName, "text-field",
                [
                    "case",
                    ["all", ["has", "floodCount"], [">", ["get", "floodCount"], 0]],
                    ["concat", ["get", "point_count_abbreviated"], "*"],
                    ["get", "point_count_abbreviated"]
                ]
            );

            // Update the icon-image content
            map.setLayoutProperty(layerName, "icon-image",
                [
                    "case",
                    ["all", ["has", "floodCount"], [">", ["get", "floodCount"], 0]],
                    ["string", "error-empty"],
                    ["get", "icon-image"]
                ]
            );
        }
    });
}