# First stage: download the Java dependencies (allows them to be cached if unchanged)
#==================================================================================================
FROM maven:3.6-openjdk-11 as retriever

# Copy in Maven settings templates and credentials 
COPY docker/credentials /root/credentials
COPY docker/.m2 /root/.m2

# Populate settings templates with credentials
WORKDIR /root/.m2
# (Note that | rather than / is used as the sed delimiter, since encrypted passwords can contain the former, but not the latter
RUN sed -i "s|MASTER_PASSWORD|$(mvn --encrypt-master-password master_password)|" settings-security.xml
RUN sed -i "s|REPO_USERNAME|$(cat ../credentials/repo_username.txt)|;s|REPO_PASSWORD|$(cat ../credentials/repo_password.txt|xargs mvn --encrypt-password)|" settings.xml

# Copy in Java source and build jar
WORKDIR /root/credo_visualisation

COPY credo_visualisation/pom.xml ./pom.xml
RUN mvn clean dependency:resolve

#==================================================================================================

# Second stage: build war file
#==================================================================================================

FROM maven:3.6-openjdk-11-slim as builder

COPY --from=retriever /root/.m2 /root/.m2

# Copy in Java source and build jar
WORKDIR /root/credo_visualisation

COPY credo_visualisation .
RUN mvn install -DskipTests

#==================================================================================================

# Third stage: copy the downloaded dependency into a new image and build into an app
#==================================================================================================
FROM openjdk:11-jre-slim as agent

WORKDIR /app

# Copy the compiled jar from the builder
COPY --from=builder /root/credo_visualisation/target/*.jar /app
# Copy the downloaded dependencies from the builder
COPY --from=builder /root/credo_visualisation/target/lib /app/lib

# Copy in entrypoint script
COPY ./docker/entrypoint.sh ./sanitise_data.sh ./rename_files.sh /app/

# Sanitise the data by default
ENV SANITISE_DATA="TRUE"

# Run the jar, via the entrypoint script
ENTRYPOINT /app/entrypoint.sh
