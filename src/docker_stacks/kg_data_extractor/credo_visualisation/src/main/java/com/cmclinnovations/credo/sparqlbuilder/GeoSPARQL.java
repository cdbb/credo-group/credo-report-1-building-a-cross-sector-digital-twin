package com.cmclinnovations.credo.sparqlbuilder;

import org.eclipse.rdf4j.sparqlbuilder.constraint.Operand;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;

public final class GeoSPARQL {

	public static Operand sfContains(Variable a, Variable b) {
		return () -> "geof:sfContains(" + a.getQueryString() + "," + b.getQueryString() + ")";
	}
}
