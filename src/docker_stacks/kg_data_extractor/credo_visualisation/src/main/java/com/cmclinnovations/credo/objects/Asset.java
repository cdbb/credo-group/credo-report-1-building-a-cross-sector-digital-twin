package com.cmclinnovations.credo.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.cmclinnovations.credo.objects.Connection.ConnectionType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.json.JSONObject;

import uk.ac.cam.cares.jps.base.timeseries.TimeSeries;

public class Asset {
    private static final Logger LOGGER = LogManager.getLogger(Asset.class);
    private String iri;
    private String type; // rdf type
    
    // display name of site is in the form: <owner> <label> <comment> <name>
    private String name;
    private String id;
    private String label;
    private String comment;

    Map<Integer,Boolean> timestep_state_map; // if anything is broken/flooded, true = something is down, false = nothing wrong

    private int visId; // purely for visualisation to match the point to its metadata
    private JSONObject location;
    private Map<String, String> properties; // list of key-value pair to display on the side panel
    private List<Asset> parts; // asset can contain a list of assets
    private TimeSeries<Integer> statesTimeSeries; // time series of states, can be flood depth, phone state etc.
    private boolean hasStates;
    private Map<String, String> state_unit_map; // store unit of time series states, if present

    private Map<String, List<Asset>> suppliesTo; // list of assets this is supplying to with connection label as key
    private List<Connection> connections; // list of connections
    private Map<String, List<Asset>> receivesFrom; // list of assets that supply this asset with connection label as key

    // criticality score based on connections
    private double criticality = 0.0;
    private double weight = 1.0;

    public Asset(String iri) {
        this.iri = iri;
        this.parts = new ArrayList<Asset>();
        this.properties = new HashMap<>();
        this.suppliesTo = new HashMap<>();
        this.receivesFrom = new HashMap<>();
        this.timestep_state_map = new HashMap<>();
        this.connections = new ArrayList<>();
        this.state_unit_map = new HashMap<>();
    }

    public double getWeight() {
        return this.weight;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return this.comment;
    }

    public void setVisId(int visId) {
        this.visId = visId;
    }

    public int getVisId() {
        return this.visId;
    }

    public void addPart(Asset part) {
        this.parts.add(part);
    }

    public List<Asset> getParts() {
        return this.parts;
    }

    public void addProperty(String key, String value) {
        properties.put(key,value);
    }

    public List<String> getPropertyKeys() {
        return properties.keySet().stream().collect(Collectors.toList());
    }

    public String getProperty(String propkey) {
        return properties.get(propkey);
    }

    public void setLocation(JSONObject location) {
        this.location = location;
    }

    public JSONObject getLocation() {
        return this.location;
    }

    public String getIri() {
        return this.iri;
    }

    public void addSuppliesTo(Asset asset, String connLabel, Connection conn) {
        if (this.suppliesTo.containsKey(connLabel)) {
            this.suppliesTo.get(connLabel).add(asset);
        } else {
            List<Asset> newAssetList = new ArrayList<>();
            newAssetList.add(asset);
            this.suppliesTo.put(connLabel, newAssetList);
        }
        this.connections.add(conn);
    }

    public void addReceivesFrom(Asset asset, String connLabel, Connection conn) {
        if (this.receivesFrom.containsKey(connLabel)) {
            this.receivesFrom.get(connLabel).add(asset);
        } else {
            List<Asset> newAssetList = new ArrayList<>();
            newAssetList.add(asset);
            this.receivesFrom.put(connLabel, newAssetList);
        }
        this.connections.add(conn);
    }

    public List<Connection> getConnections() {
        return this.connections;
    }

    public Map<String, List<Asset>> getSuppliesTo() {
        return this.suppliesTo;
    }
    public Map<String, List<Asset>> getReceivesFrom() {
        return this.receivesFrom;
    }

    public void setStatesTimeSeries(TimeSeries<Integer> timeseries) {
        this.statesTimeSeries = timeseries;
        this.hasStates = true;
    }

    public List<String> getStates() {
        return this.statesTimeSeries.getDataIRIs();
    }

    public TimeSeries<Integer> getStateTimeSeries() {
        return this.statesTimeSeries;
    }

    public boolean hasStates() {
        return this.hasStates;
    }

    public void setState(int timestep, boolean stateError) {
        this.timestep_state_map.put(timestep, stateError);
    }

    public boolean getState(int timestep) {
        if (this.timestep_state_map.containsKey(timestep)) {
            return this.timestep_state_map.get(timestep);
        } else { 
            // false is the default value
            // if it's in the map it will be true
            return false;
        }
    }

    public void setStateUnit(String state, String unit) {
        if (this.state_unit_map.containsKey(state)) {
            if (this.state_unit_map.get(state) != unit) {
                LOGGER.error("Possible multiple units attached to the same state " + state + this.state_unit_map.get(state) + " "+ unit);
            }
        } else {
            this.state_unit_map.put(state, unit);
        }
    }

    public String getStateUnit(String state) {
        return this.state_unit_map.get(state);
    }

    public void setCriticality(double criticality) {
        this.criticality = criticality;
    }

    public double getCriticality() {
        return this.criticality;
    }

    double calcCriticality(Connection connIn, List<Asset> leftOutSites, DirectedAcyclicGraph<Asset, DefaultEdge> graph) throws Exception {
        List<Connection> connList = this.getConnections();

        graph.addVertex(this);
        double criticalityLocal = this.getWeight();

        for (Connection connOut : connList) {
            if (connOut.getConnectionType() == ConnectionType.connTypeDefault) {
                LOGGER.warn("Unknown connection type detected, calculation for criticalitities may be inaccurate");
            }

            Asset connectingSite = null;
            if (connOut.getSupplier() == this) {
                switch (connOut.getConnectionType()) {
                    case suppliesWaterTo:
                        // ignore supplies water to avoid circular dependencies
                        break;
                    case suppliesSewageTo:
                        break;
                    default:
                        connectingSite = connOut.getReceiver();
                        break;
                }
            } else if (connOut.getReceiver() == this) {
                switch (connOut.getConnectionType()) {
                    case suppliesSewageTo:
                        // sewage connections are inversed for calculating criticalities
                        connectingSite = connOut.getSupplier();
                        break;
                    default:
                        break;
                }
            }

            if (connectingSite != null) {
                final Asset finalConnectingSite = connectingSite;
                if (connIn != null && connIn.getConnectionType() != ConnectionType.suppliesPowerTo && connOut.getConnectionType() != connIn.getConnectionType()) {
                    leftOutSites.add(this);
                    return this.getWeight();
                }
                graph.addVertex(connectingSite);

                double redundancyFactor;
                switch (connOut.getConnectionType()) {
                    case suppliesSewageTo:
                        // get number of sites this connecting site is supplying sewage to
                        redundancyFactor = connectingSite.getConnections().stream().filter(c -> c.getSupplier() == finalConnectingSite && c.getConnectionType() == connOut.getConnectionType()).count();
                        break;
                    default:
                        // get the number of sites supplying to this connecting site with the same connection type
                        // e.g. number of sites supplying power to the connecting site
                        redundancyFactor = connectingSite.getConnections().stream().filter(c -> c.getReceiver() == finalConnectingSite && c.getConnectionType() == connOut.getConnectionType()).count();
                        break;
                }

                if (connectingSite.getCriticality() == 0.0) {
                    try {
                        graph.addEdge(this, connectingSite);
                    } catch (Exception e) {
                        LOGGER.error("Error in calculating criticality between " + this.getIri() + " and " + connectingSite.getIri());
                        throw new Exception (e);
                    }
                    criticalityLocal += connectingSite.calcCriticality(connOut, leftOutSites, graph) / redundancyFactor;
                } else {
                    criticalityLocal += connectingSite.getCriticality() / redundancyFactor;
                }
            }
        }
        this.setCriticality(criticalityLocal);
        return criticalityLocal;
    }
}
