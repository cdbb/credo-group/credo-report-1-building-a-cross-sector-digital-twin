package com.cmclinnovations.credo.sparqlbuilder;

import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfPredicate;

public final class PropertyPaths extends org.eclipse.rdf4j.sparqlbuilder.core.PropertyPaths {

    public static RdfPredicate inverse(RdfPredicate aElement) {
        return () -> "^" + aElement.getQueryString();
    }

    public static RdfPredicate zeroOrOne(RdfPredicate aElement) {
        return () -> aElement.getQueryString() + "?";
    }
}