package com.cmclinnovations.credo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

class FloodDataProcessor {

    private static final String TIME_REGEX = "[0-9]*";
    private static final String WORKSPACE_NAME = "credo";
    static final String FLOOD_LAYER_DISPLAY_NAME = "Flood depth";
    private static final String STYLE_NAME = "flood_data";

    private static final Path GEOSERVER_TEMPLATE_DIR = Path.of("/geoservertemplates");

    private static final Map<Path, FileSystem> ZIP_FILESYSTEM_CACHE = new HashMap<>();

    private final List<String> layerNames = new ArrayList<>();

    public FloodDataProcessor() {
    }

    private void copyIntoZip(Path input, Path output) {
        try {
            if (Files.isRegularFile(input)) {
                Files.copy(input, output, StandardCopyOption.REPLACE_EXISTING);
            } else if (Files.isDirectory(input)) {
                Files.createDirectories(output);
                try (Stream<Path> subDirStream = Files.list(input)) {
                    subDirStream.forEach(child -> copyIntoZip(child, output.resolve(child.getFileName().toString())));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to copy '" + input
                    + "' into the zip file '" + output.getFileSystem() + "'.", e);
        }
    }

    public void generateGeoserverFiles() {

        Map<String, String> variableSubstitutions = new HashMap<>();

        try {
            Files.createDirectories(Config.GEOTIF_OUT_DIR);
        } catch (IOException e) {
            throw new RuntimeException(
                    "Failed to create directory '" + Config.GEOTIF_OUT_DIR + "' for Geoserver files.",
                    e);
        }

        // Workspace files
        variableSubstitutions.put("${WORKSPACE_NAME}", WORKSPACE_NAME);

        extractResourcesWithSubstitution(List.of("workspace.json", STYLE_NAME + ".sld"), GEOSERVER_TEMPLATE_DIR,
                Config.GEOTIF_OUT_DIR, variableSubstitutions);

        // Store and configure layer files
        variableSubstitutions.put("${TIME_REGEX}", TIME_REGEX);
        variableSubstitutions.put("${STYLE_NAME}", STYLE_NAME);

        List<String> zipContentfileNames = List.of("indexer.properties", "timeregex.properties");
        List<String> configFileNames = List.of("coverageconfig.json", "styleconfig.json");

        Path sourceDirectory = Config.GEOTIF_IN_DIR;
        if (Files.exists(sourceDirectory)) {
            System.out.println("Found geotifs input dir at " + sourceDirectory.toString());
            try (Stream<Path> sourceDirStream = Files.list(sourceDirectory)) {
                sourceDirStream.filter(Files::isDirectory).forEach(layerInDir -> {
                    System.out.println(" processing " + layerInDir.toString());
                    String layerName = layerInDir.getFileName().toString();
                    System.out.println(" set layer name as " + layerName);
                    layerNames.add(layerName);
                    Path outputZipPath = Config.GEOTIF_OUT_DIR.resolve(layerName + ".zip");

                    System.out.println(" zip output will be at " + outputZipPath.toString());
                    try {
                        System.out.println(" layer " + layerName + ": create dirs ");
                        Path layerOutDir = outputZipPath
                                .resolveSibling(FilenameUtils.getBaseName(outputZipPath.toString()));
                        Files.createDirectories(layerOutDir);

                        System.out.println(" layer " + layerName + ": sub vars");
                        variableSubstitutions.put("${LAYER_NAME}", layerName);

                        System.out.println(" layer " + layerName + ": extract resources with sub1 ");
                        extractResourcesWithSubstitution(
                                zipContentfileNames, GEOSERVER_TEMPLATE_DIR.resolve("layer"),
                                layerOutDir, variableSubstitutions);

                        System.out.println(" layer " + layerName + ": copy to zip ");
                        try (FileSystem zipfs = getZipFileSystem(outputZipPath)) {
                            copyIntoZip(layerInDir, zipfs.getPath("/"));
                            copyIntoZip(layerOutDir, zipfs.getPath("/"));
                            FileUtils.cleanDirectory(layerOutDir.toFile());
                        }

                        extractResourcesWithSubstitution(
                                configFileNames, GEOSERVER_TEMPLATE_DIR.resolve("layer"),
                                layerOutDir, variableSubstitutions);

                        System.out.println(" layer " + layerName + ": extract resources with 2 ");

                    } catch (IOException e) {
                        throw new RuntimeException("Failed to copy flood data GeoTiff files.", e);
                    }
                });
            } catch (IOException e) {
                throw new RuntimeException("Failed to copy flood data GeoTiff files.", e);
            }
        } else {
            System.out.print("Geotifs input dir (" + Config.GEOTIF_IN_DIR.toString() + ") doesn't exist.");
        }
    }

    private FileSystem getZipFileSystem(Path outputZipPath) throws IOException {
        return ZIP_FILESYSTEM_CACHE.compute(outputZipPath, (zipPath, zipfs) -> {
            if (null != zipfs && zipfs.isOpen()) {
                return zipfs;
            } else {
                Map<String, String> env = new HashMap<>();
                env.put("create", "true");
                URI uri = URI.create("jar:file:" + zipPath.toString());
                try {
                    return FileSystems.newFileSystem(uri, env);
                } catch (IOException e) {
                    throw new RuntimeException("Failed to open/create zip filesystem '" + zipPath + "'.", e);
                }
            }
        });
    }

    private void extractResourcesWithSubstitution(List<String> fileNames, Path inputDir, Path outputDir,
            Map<String, String> variableSubstitutions) {

        String[] keys = variableSubstitutions.keySet().toArray(new String[0]);
        String[] values = variableSubstitutions.values().toArray(new String[0]);

        fileNames.forEach(fileName -> {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                    getClass().getResourceAsStream(inputDir.resolve(fileName).toString())));
                    BufferedWriter writer = Files.newBufferedWriter(outputDir.resolve(fileName))) {
                reader.lines()
                        .map(line -> StringUtils.replaceEach(line, keys, values))
                        .forEachOrdered(line -> {
                            try {
                                writer.append(line);
                                writer.newLine();
                            } catch (IOException e) {
                                throw new RuntimeException("Failed to write line '" + line
                                        + "'' to flood data Geoserver file '" + fileName + "'.", e);
                            }
                        });
            } catch (IOException e) {
                throw new RuntimeException("Failed to copy flood data Geoserver files.", e);
            }
        });
    }

    public JSONObject getFloodLayerNode(String layerName, Integer previousTime, Integer time) {
        JSONObject dataSet = new JSONObject();
        dataSet.put("name", FLOOD_LAYER_DISPLAY_NAME);
        dataSet.put("dataLocation", "${baseurl}/geoserver/" + WORKSPACE_NAME
                + "/wms?time="
                + previousTime + "/" + time
                + "&service=WMS&version=1.1.0&request=GetMap&layers=" + WORKSPACE_NAME
                + ":" + layerName
                + "&bbox={bbox-epsg-3857}&width=256&height=256&srs=EPSG:3857&transparent=true&format=image/png&styles=");
        dataSet.put("locationType", "raster");
        dataSet.put("dataType", "raster");
        return dataSet;
    }

    public List<String> getLayernames() {
        return layerNames;
    }
}
