# Visualisation data generator
## Requirements
See the Config.java source file for the list of environment variables that need to be set for this container.

## Overview
This script queries data from the knowledge graph and writes out data in the form of JSON and GeoJSON files into the directory specified in the `src/main/resources/credentials.properties` file. Queried information from the knowledge graph are stored in two key types of objects in this script:
1) Asset
2) Connection

### Asset
Each asset queried from the knowledge graph is instantiated as an Asset object. Properties that are stored in an Asset object:
- Name
- ID
- Coordinates
- List of assets supplying to this asset
- List of assets this asset supplies to
- Time series of state properties, e.g. telephone state, flood depth, power state
- All other properties queried via `?asset ?hasProperty/credo:hasValue ?value` where hasProperty is a subclass of http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#hasProperty. Example of properties queried here include MPAN, latitude, longitude, easthing, and northing.


### AssetList
Throughout the execution of this code, there is an AssetList object that stores the list of assets queried from the knowledge graph.

### Connection
Each connection object contains two asset objects: 1) the supplier, and 2) the receiver.

### ConnectionList
Similar to AssetList, a ConnectionList object is instantiated to store the complete list of Connection objects.

## Queries
All queries are defined in the QueryClient class. Majority of data regarding the assets are queried using SPARQL, time series data are stored in a relational database and are queried using SQL.

### initialiseAssets
SPARQL query:
```
PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
SELECT ?asset ?assetClass ?name ?id ?label ?comment 
WHERE { SERVICE <http://credo-research-vm.dafni.rl.ac.uk:1066/sparql> {?asset a ?assetClass .OPTIONAL { ?asset credo:hasName ?name . }OPTIONAL { ?asset credo:hasId ?id . }}
?assetClass <http://www.w3.org/2000/01/rdf-schema#subClassOf>* credo:Asset .
OPTIONAL { ?assetClass <http://www.w3.org/2000/01/rdf-schema#label> ?label . }
OPTIONAL { ?assetClass <http://www.w3.org/2000/01/rdf-schema#comment> ?comment . } }
```
This is the first executed query in this script to initialise the assets. This queries all assets that are subclass of the generic CReDo asset class, `http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#Asset`. The results of this query include:
1. ?asset: IRI of the asset
2. ?assetClass: Asset class (subclass of the generic CReDo asset class)
3. ?name: Name of the asset
4. ?id: ID of the asset
5. ?label: Label of the asset class, e.g. Water Site.
6. ?comment: Description of the asset class

Each asset is stored as an `Asset` object and recorded within the `AssetList` object.
### organiseSites
This queries for sites that are subclass of the generic CReDo site, `http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#Site`. At this point, all assets should have been initialised from the first query, the main purpose of this query is to organise sites according to their owners.
SPARQL query:
```
PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
SELECT ?site ?location ?owner
WHERE { SERVICE <http://credo-research-vm.dafni.rl.ac.uk:1066/sparql> {{ ?site a ?siteSubClass ;
    credo:hasOwner ?owner .
OPTIONAL { ?asset credo:hasLocation/credo:hasJSON ?location . } }}
{ ?siteSubClass <http://www.w3.org/2000/01/rdf-schema#subClassOf>* credo:Site . } }
```
Results of this query:
1) ?site: IRI of the site
2) ?location: Coordinates of the sites
3) ?owner: Owner of site

The main purpose of this query is to organise the sites according to its subtype and owner, as well as its coordinates to display it on the map. Each site is placed in a group like the following: 
```
  Owner 1
    Asset subtype 1 
      List of sites
    Asset subtype 2
      List of sites
  Owner 2
    Asset subtype 3
      List of sites
    Asset subtype 4
      List of sites
```

### getConnections
This queries for all the connections and records each connection as a Connection object. The predicate used
```
PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
SELECT ?supplier ?receiver ?suppliesSubClass ?label ?comment
WHERE { { ?suppliesSubClass <http://www.w3.org/2000/01/rdf-schema#subPropertyOf>* credo:supplies .
OPTIONAL { ?suppliesSubClass <http://www.w3.org/2000/01/rdf-schema#label> ?label . }
OPTIONAL { ?suppliesSubClass <http://www.w3.org/2000/01/rdf-schema#comment> ?comment . } }
SERVICE <http://credo-research-vm.dafni.rl.ac.uk:1066/sparql> {?supplier ?suppliesSubClass ?receiver .} }
```

Results of this query include:
1) ?supplier: IRI of the supplier (a site) in this connection
2) ?receiver: IRI of the receiver (a site) in this connection
3) ?suppliesSubClass: IRI of the `http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#supplies` subclass, e.g. supplies water, supplies electricity, etc.
3) ?label: Label
4) ?comment: Description of the supplies subclass

## Failure summary files

As well as the JSON files generated by this code there are also a set of CSV files that are accessible via the "Additional Files" tab.

The first of these is a "Failure summary file", which contains the number of assets of each type that have failed due to each potential cause.
The cause of failure is determined by looking at which of the asset's states was negative for the most timesteps.
In the case of ties involving the flood state it is assumed that flooding was the main cause of failure as it is assumed here that a flooded asset cannot supply any services.
The power state is the next one to be considered as a loss of power for an asset will prevent it from supplying any services.
The other states are then considered under the assumption that they will only affect the supply of services of the same type.

There is also a CSV file per asset owner summary files that show basic information and whether each asset has flooded and/or any of the supply states have gone down for one or more timesteps.
