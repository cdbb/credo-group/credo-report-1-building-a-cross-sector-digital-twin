#!/bin/sh

# Replace asset owner's names
grep -rl "Anglian Water\|BT Group\|UK Power Networks" "${OUTPUT_DIR}" | while read -r filename ; do sed -i -e 's/Anglian Water/Water Network/g' -e 's/BT Group/Communications Network/g' -e 's/UK Power Networks/Power Network/g' "$filename" ; done
grep -rl " AW \| BT \| UKPN " "${OUTPUT_DIR}" | while read -r filename ; do sed -i -e 's/ AW / Water Network /g' -e 's/ BT / Communications Network /g' -e 's/ UKPN / Power Network /g' "$filename" ; done
# Rename the failure summary files
find "${OUTPUT_DIR}" \( -name "Anglian Water*" -o -name "BT Group*" -o -name "UK Power Networks*" \) -exec ./rename_files.sh {} \;