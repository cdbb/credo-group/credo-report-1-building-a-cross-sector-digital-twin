import pyder_agent.comms.query_templates as qtempl
import pyder_agent.common.endpoints as endpoints
from pyder_agent.flaskapp.update_routes.update_phone_states import UPDATE_PHONE_STATES_ROUTE
from pyder_agent.derivations.derivation_client import get_derivation_client
import pyder_agent.common.tbox_concepts as tbox
from tqdm import tqdm
import logging

logger = logging.getLogger(__name__)

def add_phone_state_derivations()->None:

    derivation_client = get_derivation_client(
        query_endpoint=endpoints.ONTOP_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    logger.info("Querying for assets phone states dependencies")
    response = qtempl.get_phone_state_dependecies()
    logger.info(f"Found {len(response)} entries to be processed")
    # dictionary that would hold the following info, e.g.:
    #   (belongsTo)
    #   child_phone_state_iri =  [parent_phone_state_iri1,     # isDerivedFrom_1
    #                             parent_phone_state_iri2,     # isDerivedFrom_2
    #                             ..,                          # ...
    #                             child_power_state_iri]       # isDerivedFrom_N-1
    #                             child_flood_state_iri]       # isDerivedFrom_N
    phone_state_dep = {}
    if response:
        logger.info(f"Processing found dependency entries")
        for response_dict in tqdm(response):
            child_phone_state = response_dict['child_phone_state']
            child_type = response_dict['child_type'].strip()
            # if phone type connection not specified, assume the legacy type
            child_phone_type = response_dict.pop('child_phone_type', tbox.LEGACY_CONNECTION).strip()
            # child power and flood states dependency might be optional
            child_power_state = response_dict.pop('child_power_state', None)
            child_flood_state = response_dict.pop('child_flood_state',None)
            parent_phone_state = response_dict.pop('parent_phone_state',None)

            if child_power_state is None and \
               child_flood_state is None and \
               parent_phone_state is None:
                continue

            if child_phone_state not in phone_state_dep:
                phone_state_dep[child_phone_state] = []

            temp_list = phone_state_dep[child_phone_state]

            if parent_phone_state is not None:
                if parent_phone_state not in temp_list:
                    temp_list.append(parent_phone_state)

            if child_power_state is not None:
                if child_type == tbox.EXCHANGE_SITE or \
                  (child_type == tbox.CABINET_SITE and tbox.FIBRE_CONNECTION in child_phone_type):
                    # only add the PowerState dependency to:
                    # - all exchange sites
                    # - fibre cabinets
                    #
                    # do not add the PowerState dependency to other
                    # non BT sites, e.g. UKPN substations, this is because
                    # in all likelihood they all have the legacy connection,
                    # also data does not specify which type it is
                    if child_power_state not in temp_list:
                        temp_list.append(child_power_state)

            if child_flood_state is not None:
                if child_flood_state not in temp_list:
                    temp_list.append(child_flood_state)

            phone_state_dep[child_phone_state] = temp_list
            if not phone_state_dep[child_phone_state]:
                # this is needed for non bt sites that do not depend on power
                # by default
                phone_state_dep.pop(child_phone_state)

    logger.info(f"Found {len(phone_state_dep)} assets phone states dependent on other states")
    if phone_state_dep:
        logger.info(f"Adding derivations to these assets")
        # init outer lists for bulk creation of derivations
        phone_state_iris = []     # child phone state iri list of lists
        agent_iris = []           # agent iris list
        phone_state_dep_iris = [] # child phone state dependency list (isDerivedFrom)
        agent_urls = []           # agent urls list
        for child_phone_state, child_phone_state_dep in phone_state_dep.items():
            phone_state_iris.append([child_phone_state])
            agent_iris.append(child_phone_state+'/agent')
            phone_state_dep_iris.append(child_phone_state_dep)
            agent_urls.append(endpoints.UPDATE_AGENT_HTTP+UPDATE_PHONE_STATES_ROUTE)

        # bulk create phone state derivations
        _ = derivation_client.bulkCreateDerivationsWithTimeSeries(
            belongs_to_iris=phone_state_iris,
            agent_iris=agent_iris,
            derived_from_iris=phone_state_dep_iris,
            update_agent_urls=agent_urls
        )