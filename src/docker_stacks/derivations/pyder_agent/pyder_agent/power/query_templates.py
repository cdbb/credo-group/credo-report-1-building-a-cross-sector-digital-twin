from pyder_agent.kgoperations.store_client import get_store_client
import pyder_agent.common.endpoints as endpoints
from typing import List, Dict, Any

def get_asset_power_states()->List[Dict[str,Any]]:

    query = """
        PREFIX ukpn: <http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#>
        SELECT DISTINCT ?power_state
        WHERE {
            ?power_state a ukpn:PowerState .
        }"""
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response

def get_power_state_dependecies():
    query = """
    PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#>
    PREFIX ukpn: <http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#>
    SELECT ?child ?child_power_state ?child_flood_state ?parent ?parent_power_state
    WHERE {
        ?child ukpn:hasPowerState ?child_power_state .

        OPTIONAL {?parent ukpn:suppliesPowerTo ?child ;
                ukpn:hasPowerState ?parent_power_state . }

        OPTIONAL {?child credo:hasFloodState ?child_flood_state .}
    }
    """
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response

def get_root_power_states()->List[Dict[str,Any]]:
    query = """
        PREFIX ukpn: <http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#>
        SELECT DISTINCT ?root_power_state
        WHERE {
            ?root_asset ukpn:suppliesPowerTo ?child_asset ;
                        ukpn:hasPowerState ?root_power_state .

            MINUS {?other_asset ukpn:suppliesPowerTo ?root_asset .}
        }
        """
    store_client = get_store_client(query_endpoint=endpoints.ONTOP_SPARQL)
    response = store_client.executeQuery(query)
    return response


def get_leaf_power_state_derivations()->List[Dict[str,Any]]:
    query = """
        PREFIX der: <https://github.com/cambridge-cares/TheWorldAvatar/blob/develop/JPS_Ontology/ontology/ontoderivation/OntoDerivation.owl#>
        PREFIX ukpn: <http://theworldavatar.com/ontology/ontocredo/ontoukpn.owl#>
        SELECT DISTINCT ?leaf_power_state_derivation
        WHERE {
            { SELECT ?leaf_power_state
                WHERE {

                BIND (?leaf_power_state_in AS ?leaf_power_state)
                SERVICE <#ontop_endpoint#> {
                    ?asset ukpn:suppliesPowerTo ?leaf_asset .
                    ?leaf_asset ukpn:hasPowerState ?leaf_power_state_in .
                    MINUS {?leaf_asset ukpn:suppliesPowerTo ?other_asset .}
                }
                }
            }
            ?leaf_power_state der:belongsTo ?leaf_power_state_derivation .

        }""".replace('#ontop_endpoint#', endpoints.ONTOP_SPARQL)

    store_client = get_store_client(query_endpoint=endpoints.BLAZEGRAPH_SPARQL)
    response = store_client.executeQuery(query)
    return response