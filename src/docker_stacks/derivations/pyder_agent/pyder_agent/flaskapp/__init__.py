from flask import Flask

def create_app(test_config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    app.config['JSON_SORT_KEYS'] = False
    if test_config is not None:
        # load the test config if passed in
        app.config.update(test_config)

    with app.app_context():
        # Import parts of our application
        import pyder_agent.flaskapp.home.routes as home
        import pyder_agent.flaskapp.update_routes.update_flood_states as update_flood_states
        import pyder_agent.flaskapp.update_routes.update_power_states as update_power_states
        import pyder_agent.flaskapp.update_routes.update_phone_states as update_phone_states
        import pyder_agent.flaskapp.update_routes.update_water_states as update_water_states
        import pyder_agent.flaskapp.update_routes.update_sewage_states as update_sewage_states
        import pyder_agent.flaskapp.update_routes.update_sludge_states as update_sludge_states


        # Register Blueprints
        app.register_blueprint(home.home_bp)
        app.register_blueprint(update_flood_states.update_flood_states_bp)
        app.register_blueprint(update_power_states.update_power_states_bp)
        app.register_blueprint(update_phone_states.update_phone_states_bp)
        app.register_blueprint(update_water_states.update_water_states_bp)
        app.register_blueprint(update_sewage_states.update_sewage_states_bp)
        app.register_blueprint(update_sludge_states.update_sludge_states_bp)

    return app