from flask import Blueprint
import pyder_agent.common.endpoints as endpoints


home_bp = Blueprint(
    'home_bp', __name__
)

# Define a route for API requests
# should be called update_state
@home_bp.route('/', methods=['GET'])
def api():
    msg =  "pyder agent home route \n"
    msg += "pyder agent endpoints variables: \n"
    msg += f"BLAZEGRAPH_SPARQL: {endpoints.BLAZEGRAPH_SPARQL}\n"
    msg += f"ONTOP_SPARQL: {endpoints.ONTOP_SPARQL}\n"
    msg += f"POSTGRES_URL: {endpoints.POSTGRES_URL}\n"
    return msg