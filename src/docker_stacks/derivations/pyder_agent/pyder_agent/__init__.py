import logging

logging.getLogger('py4j').propagate = False

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s [%(threadName)s] [%(levelname)s] %(message)s',
                    handlers=[logging.StreamHandler()])