import os
from pyder_agent.comms.phone_state_derivations import add_phone_state_derivations
from pyder_agent.power.power_state_derivations import add_power_state_derivations
from pyder_agent.flood.flood_state_derivations import add_flood_state_derivations
from pyder_agent.water.water_state_derivations import add_water_state_derivations
from pyder_agent.water.sewage_state_derivations import add_sewage_state_derivations
from pyder_agent.water.sludge_state_derivations import add_sludge_state_derivations
from pyder_agent.derivations.derivation_client import drop_all_derivations
from pyder_agent.derivations.init_root_time_stamps import init_root_states_time_stamps
from pyder_agent.derivations.validate_derivations import validate_derivations
from pyder_agent.time_series.add_state_timeseries import add_time_series_to_all_states
from pyder_agent.time_series.time_series_client import drop_all_time_series
from pyder_agent.common.query_templates import drop_manual_state_overrides
from pyder_agent.common.simulation_time import create_simulation_time_series
import logging

logger = logging.getLogger(__name__)

def add_derivations():
    try:
        logger.info("----------Removing all derivations----------")
        drop_all_derivations()
        logger.info("----------Removing all time series----------")
        drop_all_time_series()
        logger.info("----------Removing all manual overrides----------")
        drop_manual_state_overrides()
        logger.info("----------Creating simulation_time time series----------")
        create_simulation_time_series()

        logger.info("----------Adding time series to all assets states----------")
        add_time_series_to_all_states()
        logger.info("----------Adding flood state derivations----------")
        add_flood_state_derivations()
        logger.info("----------Adding power state derivations----------")
        add_power_state_derivations()
        logger.info("----------Adding phone state derivations----------")
        add_phone_state_derivations()
        logger.info("----------Adding water state derivations----------")
# Commented out due to circular dependencies (a -> b and b -> a)
#        add_water_state_derivations()
        logger.info("----------Adding sewage state derivations----------")
        add_sewage_state_derivations()
        logger.info("----------Adding sludge state derivations----------")
        add_sludge_state_derivations()
        #add_radio_state_derivations()
        logger.info("----------Initialising root states time stamps----------")
        init_root_states_time_stamps()
        logger.info("----------Validating all derivations----------")
        validate_derivations()

    except Exception as ex:
        logger.error("Error: Adding derivations has failed. See the exception traceback for more details")
        logger.exception(ex)
        os._exit(1)