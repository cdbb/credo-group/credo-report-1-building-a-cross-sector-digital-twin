from pyder_agent.kgoperations.java_gateway import jpsBaseLibGW
from pyder_agent.app_exceptions.app_exceptions import StoreClientUpdateEndpointNotSetError
from typing import Union, Any, List, Dict
import textwrap
import json

jpsBaseLib_view = jpsBaseLibGW.createModuleView()
jpsBaseLibGW.importPackages(jpsBaseLib_view,"uk.ac.cam.cares.jps.base.query.*")

class StoreClient:
    def __init__(
        self,
        query_endpoint:str,
        update_endpoint:Union[str,None]):

        self.query_endpoint = query_endpoint
        self.update_endpoint = update_endpoint

        if update_endpoint is not None:
            self.store_client = jpsBaseLib_view.RemoteStoreClient(
                query_endpoint,
                update_endpoint)
        else:
            self.store_client = jpsBaseLib_view.RemoteStoreClient(
                query_endpoint)

    def executeQuery(self, query_str: str)-> List[Dict[str,Any]]:
        response = self.store_client.executeQuery(query_str)
        return json.loads(str(response))

    def executeUpdate(self, query_str: str)-> int:
        if self.update_endpoint is None:
            raise StoreClientUpdateEndpointNotSetError(textwrap.dedent("""
            Error: Store client update endpoint is not set. Failed to execute the update query.
            """))
        else:
            status_code: int = self.store_client.executeUpdate(query_str)
        return status_code

def get_store_client(
    query_endpoint:str,
    update_endpoint:Union[str,None]=None)->StoreClient:

    store_client = StoreClient(query_endpoint, update_endpoint)
    return store_client