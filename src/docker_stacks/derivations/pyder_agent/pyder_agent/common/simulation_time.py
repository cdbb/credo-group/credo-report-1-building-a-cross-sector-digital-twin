from pyder_agent.time_series.time_series_client import get_default_time_series_client
import pyder_agent.common.endpoints as endpoints
from pyder_agent.common.java_utils import JAVA_INTEGER_TYPE

SIMULATION_TIME_IRI="http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#simulation_time"

def create_simulation_time_series():
    time_series_client = get_default_time_series_client()

    time_series_client.add_time_series(
        data_iris=[SIMULATION_TIME_IRI],
        data_iri_classes=[JAVA_INTEGER_TYPE],
        time_stamps=[0],
        iri_values=[[0]])