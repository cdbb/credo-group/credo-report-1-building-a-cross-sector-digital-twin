import os

BLAZEGRAPH_SPARQL = "http://"+ os.environ['BLAZEGRAPH_HOST']+":"+os.environ['BLAZEGRAPH_PORT']+"/blazegraph/namespace/kb/sparql"
ONTOP_SPARQL = "http://"+os.environ['ONTOP_HOST']+":"+os.environ['ONTOP_PORT']+"/sparql"
UPDATE_AGENT_HTTP = "http://"+os.environ['UPDATE_AGENT_HOST']+":"+os.environ['UPDATE_AGENT_PORT'];

POSTGRES_URL = "jdbc:postgresql://"+ os.environ['POSTGRES_HOST']+":"+os.environ['POSTGRES_PORT']+"/credo_timeseries"
POSTGRES_USER = os.environ['POSTGRES_USER']
POSTGRES_PASSWORD = os.environ['POSTGRES_PASSWORD']
