import pyder_agent.common.endpoints as endpoints
from pyder_agent.derivations.derivation_client import get_derivation_client
from pyder_agent.app_exceptions.app_exceptions import DerivationSetupError
import textwrap
import logging

logger = logging.getLogger(__name__)

def validate_derivations():
    derivation_client = get_derivation_client(
        query_endpoint=endpoints.ONTOP_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    try:
        derivation_client.validateDerivations()
    except Exception as ex:
        raise DerivationSetupError(textwrap.dedent("""
            Error: Derivation validation error. Derivations not setup correctly.
            """)) from ex
    else:
        logger.info("Validation completed successfully")