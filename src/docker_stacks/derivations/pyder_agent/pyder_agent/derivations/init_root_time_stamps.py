import pyder_agent.common.query_templates as qtempl
import pyder_agent.common.endpoints as endpoints
from pyder_agent.derivations.derivation_client import get_derivation_client
import logging

logger = logging.getLogger(__name__)

def init_root_states_time_stamps():
    derivation_client = get_derivation_client(
        query_endpoint=endpoints.ONTOP_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    logger.info("Querying for all the root states")
    response = qtempl.get_root_states()
    logger.info(f"Found {len(response)} root states")
    root_power_states = []
    if response:
        for response_dict in response:
            root_power_states.append(response_dict['root_state'])

        logger.info(f"Adding timestamps to {len(response)} root states")
        derivation_client.addTimeInstance(iris=root_power_states)