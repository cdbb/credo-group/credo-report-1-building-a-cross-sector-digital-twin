import pyder_agent.water.query_templates as qtempl
import pyder_agent.common.endpoints as endpoints
from pyder_agent.flaskapp.update_routes.update_sewage_states import UPDATE_SEWAGE_STATES_ROUTE
from pyder_agent.derivations.derivation_client import get_derivation_client
from tqdm import tqdm
import logging

logger = logging.getLogger(__name__)

def add_sewage_state_derivations()->None:

    derivation_client = get_derivation_client(
        query_endpoint=endpoints.ONTOP_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    logger.info("Querying for assets sewage states dependencies")
    response = qtempl.get_sewage_state_dependecies()
    logger.info(f"Found {len(response)} entries to be processed")
    # dictionary that would hold the following info, e.g.:
    #   (belongsTo)
    #   child_sewage_state_iri =  [parent_sewage_state_iri1,     # isDerivedFrom_1
    #                             parent_sewage_state_iri2,      # isDerivedFrom_2
    #                             ..,                            # ...
    #                             child_power_state_iri]         # isDerivedFrom_N-1
    #                             child_flood_state_iri]         # isDerivedFrom_N
    sewage_state_dep = {}
    if response:
        logger.info(f"Processing found dependency entries")
        for response_dict in tqdm(response):
            child_sewage_state = response_dict['child_sewage_state']
            # child power and flood states dependency might be optional
            child_power_state = response_dict.pop('child_power_state', None)
            child_flood_state = response_dict.pop('child_flood_state',None)
            parent_sewage_state = response_dict.pop('parent_sewage_state', None)

            if child_power_state is None and child_flood_state is None and \
               parent_sewage_state is None:
                continue

            if child_sewage_state not in sewage_state_dep:
                sewage_state_dep[child_sewage_state] = []

            temp_list = sewage_state_dep[child_sewage_state]

            if parent_sewage_state is not None:
                if parent_sewage_state not in temp_list:
                    temp_list.append(parent_sewage_state)

            if child_power_state is not None:
                if child_power_state not in temp_list:
                    temp_list.append(child_power_state)

            if child_flood_state is not None:
                if child_flood_state not in temp_list:
                    temp_list.append(child_flood_state)
            sewage_state_dep[child_sewage_state] = temp_list

    logger.info(f"Found {len(sewage_state_dep)} assets sewage states dependent on other states")
    if sewage_state_dep:
        logger.info(f"Adding derivations to these assets")
        # init outer lists for bulk creation of derivations
        sewage_state_iris = []     # child sewage state iri list of lists
        agent_iris = []            # agent iris list
        sewage_state_dep_iris = [] # child sewage state dependency list (isDerivedFrom)
        agent_urls = []            # agent urls list
        for child_sewage_state, child_sewage_state_dep in sewage_state_dep.items():
            sewage_state_iris.append([child_sewage_state])
            agent_iris.append(child_sewage_state+'/agent')
            sewage_state_dep_iris.append(child_sewage_state_dep)
            agent_urls.append(endpoints.UPDATE_AGENT_HTTP+UPDATE_SEWAGE_STATES_ROUTE)

        # bulk create sewage state derivations
        _ = derivation_client.bulkCreateDerivationsWithTimeSeries(
            belongs_to_iris=sewage_state_iris,
            agent_iris=agent_iris,
            derived_from_iris=sewage_state_dep_iris,
            update_agent_urls=agent_urls
        )