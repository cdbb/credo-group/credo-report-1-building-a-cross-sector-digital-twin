import pyder_agent.flood.query_templates as qtempl
import pyder_agent.common.endpoints as endpoints
from pyder_agent.flaskapp.update_routes.update_flood_states import UPDATE_FLOOD_STATES_ROUTE
from pyder_agent.derivations.derivation_client import get_derivation_client
import logging

logger = logging.getLogger(__name__)

def add_flood_state_derivations()->None:

    derivation_client = get_derivation_client(
        query_endpoint=endpoints.ONTOP_SPARQL,
        update_endpoint=endpoints.BLAZEGRAPH_SPARQL)

    logger.info("Querying for assets flood states")
    response = qtempl.get_asset_flood_states()

    if not response:
        return

    # it is assumed that the asset's FloodState only depends on
    # the assets FloodDepthState and FloodMaximumDepthStates
    # so no outgoing dependencies from other assets
    # init outer lists for bulk creation of derivations
    flood_state_iris = []     # child flood state iri list of lists
    flood_state_dep_iris = [] # child flood state dependency list (isDerivedFrom)
    agent_iris = []           # agent iris list
    agent_urls = []           # agent urls list
    logger.info(f"Adding {len(response)} flood state derivations.")
    for state_dict in response:
        # it is assumed that the three flood states must always exist together
        flood_state_iri = state_dict['flood_state']
        flood_state_depth_iri = state_dict['flood_state_depth']
        flood_state_max_depth_iri=state_dict['flood_state_max_depth']

        flood_state_iris.append([flood_state_iri])
        flood_state_dep_iris.append([flood_state_depth_iri, flood_state_max_depth_iri])
        agent_iris.append(flood_state_iri+'/agent')
        agent_urls.append(endpoints.UPDATE_AGENT_HTTP+UPDATE_FLOOD_STATES_ROUTE)

    # bulk create flood state derivations
    _ = derivation_client.bulkCreateDerivationsWithTimeSeries(
        belongs_to_iris=flood_state_iris,
        agent_iris=agent_iris,
        derived_from_iris=flood_state_dep_iris,
        update_agent_urls=agent_urls
    )