#!/bin/sh
stack=$1
shift
# The "--podman-build-args" argument requires podman compose version 0.1.8
pip3 install --user 'podman-compose==0.1.8'
podman-compose --podman-build-args="--security-opt label=disable" -f docker-compose.${stack}.yml build $@
