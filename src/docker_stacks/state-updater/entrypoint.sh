#!/bin/sh

set -e

if [ -z "$NEW_VALUES_FILENAME" ]; then
    if [ -n "$RUN_ALL" ]; then
        IFS=","
        for timestep in $SIMULATION_TIMES; do
            echo starting timestep $timestep
            java -jar /app/state-updater-1.0.0-SNAPSHOT.jar
            echo finished timestep $timestep
        done
        exit 0
    fi
fi

java -jar /app/state-updater-1.0.0-SNAPSHOT.jar
