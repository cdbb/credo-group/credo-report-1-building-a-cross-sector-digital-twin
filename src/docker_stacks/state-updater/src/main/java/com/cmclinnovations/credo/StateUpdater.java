
package com.cmclinnovations.credo;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.opencsv.CSVReader;

import org.apache.commons.io.FilenameUtils;
import org.jooq.Record;
import org.jooq.ResultQuery;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import uk.ac.cam.cares.jps.base.derivation.DerivationClient;
import uk.ac.cam.cares.jps.base.query.RemoteStoreClient;
import uk.ac.cam.cares.jps.base.timeseries.TimeSeries;
import uk.ac.cam.cares.jps.base.timeseries.TimeSeriesClient;

public class StateUpdater {

    static Logger logger = org.slf4j.LoggerFactory.getLogger(StateUpdater.class);

    public static void main(String[] args) {

        // Intialise the triple store client
        RemoteStoreClient storeClient = new RemoteStoreClient(Config.blazegraphUrl, Config.blazegraphUrl);
        // Intialise the time series client
        TimeSeriesClient<Integer> timeSeriesClient = new TimeSeriesClient<>(storeClient, Integer.class,
                Config.rdbUrl, Config.rdbUser, Config.rdbPassword);
        // Intialise the derivation client
        DerivationClient derivClient = new DerivationClient(storeClient);

        // Retrieve the value of the latest timestep
        Integer simTime = Double.valueOf(timeSeriesClient.getMaxValue(Config.simTimeIri)).intValue();

        // The IRIs of the variables that will be updated based on an external source
        List<String> updatedIris = new ArrayList<>();

        // If processing the output of an external model read the name of the model's
        // output file
        String newValuesDirectory = System.getenv("NEW_VALUES_DIR");

        // If no file is specified assume we are at the start of a new timestep
        if (null == newValuesDirectory || newValuesDirectory.isBlank()) {

            // Times should be passed as a comma-separated list of integers
            String allSimTimesString = System.getenv("SIMULATION_TIMES");
            logger.info("Parsing SIMULATION_TIMES parameter: " + allSimTimesString);
            List<Integer> allSimTimes = Stream.of(allSimTimesString.split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());

            // Get the time at the new timestep
            Integer newSimTime = allSimTimes.get(allSimTimes.indexOf(simTime) + 1);

            // Add the new time to the "simulation_time" TimeSeries, added it as both the
            // time and the value for simplicity
            TimeSeries<Integer> newSimTimeTimeSeries = new TimeSeries<Integer>(
                    List.of(newSimTime),
                    List.of(Config.simTimeIri),
                    List.of(List.of(newSimTime)));
            timeSeriesClient.addTimeSeriesData(newSimTimeTimeSeries);

            // Initilise client to extract the flood depths from the RDB
            FloodRDBClient rdbClient = new FloodRDBClient(Config.rdbUrl_credo, Config.rdbUser,
                    Config.rdbPassword);

            // Query the triple store for the locations of the sites
            String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                    + "PREFIX credo: <http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#> \n"
                    + "PREFIX mer: <http://www.theworldavatar.com/ontology/meta_model/mereology/mereology.owl#> \n"
                    + "SELECT ?fd ?e ?n \n"
                    + "WHERE {{\n"
                    + "SELECT ?x0 \n"
                    + "WHERE { \n"
                    + "?x0 <http://www.w3.org/2000/01/rdf-schema#subClassOf>* credo:Site . \n"
                    + "}} \n"
                    + "BIND(?fd_ AS ?fd) \n"
                    + "SERVICE <" + Config.ontopURL + "> { \n"
                    + "?x1 a ?x0 ; credo:hasFloodDepth ?fd_ ; credo:hasLocation ?l. "
                    + "?l	credo:hasEasting ?e ; credo:hasNorthing ?n . \n"
                    + "}}";
            JSONArray response = storeClient.executeQuery(query);

            // Concatenate the flood depth IRIs with their site's location for the next
            // query
            String coords = IntStream.range(0, response.length()).mapToObj(response::getJSONObject)
                    .map(jsonObj -> "'" + jsonObj.getString("fd") + "'," + jsonObj.getString("e")
                            + "," + jsonObj.getString("n"))
                    .collect(Collectors.joining("),(", "(", ")"));

            // SQL query for getting the flood depth at the location of each site
            String floodDepthQueryTemplate = "WITH points (fd, po1) AS (\n" +
                    "  WITH pairs(fd,x,y) AS (\n" +
                    "    VALUES " + coords + "\n" +
                    "  )\n" +
                    "  SELECT fd, ST_SetSRID(ST_Point(x,y),27700) as po1\n" +
                    "  FROM pairs\n" +
                    "),\n" +
                    "floods_with_times (rast, time) AS (\n" +
                    "SELECT rast, TO_NUMBER((regexp_match(filename, '[0-9]+'))[1], '9999999') as time\n"
                    +
                    "FROM flood_data\n" +
                    ")\n" +
                    "SELECT\n" +
                    "fd as iri, coalesce(ST_Value(rast, ST_SetSRID(po1, 27700)),0) AS floodDepth, time\n"
                    +
                    "FROM floods_with_times \n" +
                    "JOIN points ON time > " + simTime + " and time <= " + newSimTime
                    + " and ST_Contains(ST_ConvexHull(rast), po1)";
            ResultQuery<Record> floodDepthQuery = rdbClient.getContext()
                    .resultQuery(floodDepthQueryTemplate);

            Map<String, List<Integer>> floodTimes = new HashMap<>();
            Map<String, List<Double>> floodDepths = new HashMap<>();
            try {
                logger.info("Start querying flood depths ...");
                floodDepthQuery.execute();
                try (ResultSet rs = floodDepthQuery.fetchResultSet()) {
                    logger.info("Flood depth query completed, start updating timeseries ...");
                    // Group the times and flood depths by IRI
                    while (rs.next()) {
                        String iri = rs.getString("iri");
                        floodTimes.computeIfAbsent(iri,
                                dummy -> new ArrayList<>())
                                .add(rs.getInt("time"));
                        floodDepths.computeIfAbsent(iri,
                                dummy -> new ArrayList<>())
                                .add(rs.getDouble("floodDepth"));
                    }

                    // Put flood data into TimeSeries objects and add via the TimeSeries Client
                    // It is valid to assume that each flood depth is in a separate table
                    floodTimes.forEach((iri, times) -> {
                        if (!times.isEmpty()) {
                            TimeSeries<Integer> newFloodDepthTimeSeries = new TimeSeries<Integer>(
                                    times,
                                    List.of(iri),
                                    List.of(floodDepths.get(iri)));
                            timeSeriesClient.addTimeSeriesData(newFloodDepthTimeSeries);
                            updatedIris.add(iri);
                        }
                    });
                }
            } catch (SQLException ex) {
                logger.error("Querying flood depths failed.", ex);
            }
            logger.info("Flood state update finish.");
        } else {
            // Create the arrays to store the new values and the IRIs of the objects they
            // correspond to.
            Map<String, Object> updatedValues = new HashMap<>();

            Path newValuesDir = Path.of(newValuesDirectory);
            logger.info("Loading new values from directory " + newValuesDirectory);
            List<Path> newValuesFiles = null;
            try {
                newValuesFiles = Files.list(newValuesDir).collect(Collectors.toList());
            } catch (IOException e) {
                logger.error("Failed to list files in directory " + newValuesDir);
                logger.error(e.getMessage());
            }

            for (Path newValuesFile : newValuesFiles) {
                logger.info("Loading new values from file '{}'.",
                    newValuesFile.toAbsolutePath());
                
                String newValuesFileExtension = FilenameUtils.getExtension(newValuesFile.toString());

                // Read the new values from the specified file
                switch (newValuesFileExtension) {
                    case "json":
                        try {
                            updatedValues.putAll(new JSONObject(Files.readString(newValuesFile)).toMap());
                        } catch (Exception ex) {
                            logger.error("Failed to load new values from JSON file '{}'.",
                                newValuesFile, ex);
                        }
                        break;
                    case "csv":
                        try (var fr = new FileReader(newValuesFile.toFile(), StandardCharsets.UTF_8);
                                var reader = new CSVReader(fr)) {
                            reader.forEach(nextLine -> updatedValues.put(nextLine[0], nextLine[1]));
                        } catch (IOException ex) {
                            logger.error("Failed to load new values from CSV file '{}'.",
                                newValuesFile, ex);
                        }
                        break;
                    default:
                        logger.warn("File with updated values must be in either CSV (.csv) or JSON (.json) format. Ignoring "+ newValuesFile);
                }
            }

            if (!updatedValues.isEmpty()) {
                // Group the new values by their TimeSeries they are stored in
                Map<String, Map<String, Object>> valuesGroupedByTable = updatedValues.entrySet().stream()
                        .collect(Collectors.groupingBy(entry -> timeSeriesClient.getTimeSeriesIRI(entry.getKey()),
                                Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1,
                                        LinkedHashMap::new)));

                // Add externally modified values to TimeSeries RDB
                List<Integer> simTimeInList = List.of(simTime);
                Map<Class<?>, Method> valueOfMethods = new HashMap<>();
                valuesGroupedByTable.forEach(
                        // Loop over each table in the time series RDB
                        (tsIRI, entriesInTable) -> {
                            // Fix the order of the entries by using Lists
                            List<String> dataIRIsInTable = new ArrayList<>(entriesInTable.keySet());
                            List<List<?>> valuesInTable = new ArrayList<>(entriesInTable.size());
                            // Use the existing values in the table to get their types so that the new
                            // values can be cast correctly
                            TimeSeries<Integer> existingTimeSeries = timeSeriesClient.getTimeSeries(dataIRIsInTable);
                            for (String dataIRI : dataIRIsInTable) {
                                Class<?> valueClass = existingTimeSeries.getValues(dataIRI).get(0).getClass();
                                Object strValue = entriesInTable.get(dataIRI);
                                if (valueClass.isAssignableFrom(strValue.getClass())) {
                                    // Don't need to convert value
                                    valuesInTable.add(List.of(strValue));
                                } else {
                                    // Need to convert the value using the class's "valueOf" method
                                    try {
                                        Method valueOfMethod = valueOfMethods.computeIfAbsent(
                                                valueClass, StateUpdater::getvalueOfMethod);
                                        valuesInTable.add(List.of(valueOfMethod.invoke(null, strValue)));
                                    } catch (Exception ex) {
                                        logger.error("Failed to convert value '{}' to type '{}'.", strValue,
                                                valueClass.getName(), ex);
                                    }
                                }
                            }
                            timeSeriesClient.addTimeSeriesData(
                                    new TimeSeries<Integer>(simTimeInList, dataIRIsInTable, valuesInTable));
                        });

                // Add IRIs to the list so that their timestamps get updated
                updatedIris.addAll(updatedValues.keySet());
            }
        }

        if (!updatedIris.isEmpty()) {
            // Update timestamps for directly modified variables
            derivClient.updateTimestamps(updatedIris);

            // Set the 'UPDATE_DERIVATIONS' environment variable to "TRUE" to enable the
            // internal state propagation
            boolean updateDerivations = Boolean.parseBoolean(System.getenv("UPDATE_DERIVATIONS"));
            if (updateDerivations) {
                // Call the derivation framework to propagate the effects of the changes
                derivClient.updateDerivations();
            }
        } else {
            logger.warn("No variables were updated.");
        }

    }

    // Method for getting the "valueOf" method for the 'clazz' class
    private static Method getvalueOfMethod(Class<?> clazz) {
        try {
            return clazz.getMethod("valueOf", String.class);
        } catch (NoSuchMethodException | SecurityException e) {
            throw new RuntimeException(e);
        }
    }
}