package com.cmclinnovations.credo;

public class Config {
    public final static String blazegraphUrl = "http://" + System.getenv("BLAZEGRAPH_HOST") + ":"
            + System.getenv("BLAZEGRAPH_PORT") + "/blazegraph/namespace/kb/sparql";
    public final static String ontopURL = "http://" + System.getenv("ONTOP_HOST") + ":" + System.getenv("ONTOP_PORT")
            + "/sparql";
    public final static String rdbUrl = "jdbc:postgresql://" + System.getenv("POSTGRES_HOST") + ":"
            + System.getenv("POSTGRES_PORT") + "/credo_timeseries";
    public final static String rdbUrl_credo = "jdbc:postgresql://" + System.getenv("POSTGRES_HOST") + ":"
    + System.getenv("POSTGRES_PORT") + "/credo";
    public final static String rdbUser = System.getenv("POSTGRES_USER");
    public static final String rdbPassword=System.getenv("POSTGRES_PASSWORD");

    public static final String simTimeIri = "http://theworldavatar.com/ontology/ontocredo/ontocredo.owl#simulation_time";

}
