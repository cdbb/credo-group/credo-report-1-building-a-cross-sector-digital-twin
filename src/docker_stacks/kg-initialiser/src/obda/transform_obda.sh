#!/bin/bash
pushd $(dirname "$0")
input_dir=$1
output_dir=$2
mkdir -p "$output_dir"
sed -e '/# /d' -e '/^ *#/d' "${input_dir}/credo_with_comments.obda" | sed -e ':a' -e 'N' -e '$!ba' -e 's/; *\r*\n  */; /g' -e 's/\. *\r*\n  */. /g' > ${output_dir}/credo.obda
popd
