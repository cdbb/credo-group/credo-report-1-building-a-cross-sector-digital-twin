
package com.cmclinnovations.credo;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.TurtleDocumentFormat;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.OWLOntologyMerger;

import org.slf4j.Logger;

import uk.ac.cam.cares.jps.base.converter.TBoxGeneration;
import uk.ac.cam.cares.jps.base.query.RemoteStoreClient;

public class TBoxUploader {

    static Logger logger = org.slf4j.LoggerFactory.getLogger(TBoxUploader.class);

    public static void main(String[] args) {

        final String endpointURL = "http://" + System.getenv("BLAZEGRAPH_HOST") + ":" + System.getenv("BLAZEGRAPH_PORT")
                + "/blazegraph/namespace/kb/sparql";

        TBoxGeneration tBoxGenerator = new TBoxGeneration();

        RemoteStoreClient remoteStoreClient = new RemoteStoreClient(endpointURL, endpointURL);

        OWLOntologyManager inManager = OWLManager.createOWLOntologyManager();

        String[] inputDirs = (args.length != 0) ? args : System.getenv("INPUT_DIRS").split(" ");

        Path outputDir = Paths.get(System.getenv("TBOX_OUTPUT_DIR")).toAbsolutePath();
        outputDir.toFile().mkdirs();

        for (String arg : inputDirs) {
            try {
                Path folder = Paths.get(arg);
                File absFolderPath = folder.toAbsolutePath().toFile();
                if (!absFolderPath.exists()) {
                    throw new NotDirectoryException("Folder '" + absFolderPath + "' does not exist.");
                }
                File[] csvFiles = absFolderPath.listFiles((dir, name) -> name.endsWith(".csv"));

                for (File csvFile : csvFiles) {
                    Path owlFilePath = outputDir.resolve(csvFile.getName().replace(".csv", ".owl")).toAbsolutePath();
                    tBoxGenerator.generateTBox(csvFile.getAbsolutePath().toString(), owlFilePath.toString());

                    File owlFile = owlFilePath.toFile();
                    try {
                        inManager.loadOntologyFromOntologyDocument(owlFile);
                        try {
                            remoteStoreClient.uploadFile(owlFile);
                        } catch (Exception ex) {
                            logger.warn("Failed to upload ontology file '{}' to the endpoint at '{}'.", owlFilePath,
                                    endpointURL, ex);
                        }
                    } catch (OWLOntologyCreationException ex) {
                        logger.warn("Failed to load ontology file '{}'.", owlFilePath, ex);
                    }
                }
            } catch (InvalidPathException | NotDirectoryException ex) {
                logger.warn("Argument '{}' is not a valid folder path.", arg, ex);
            }
        }

        OWLOntologyManager outManager = OWLManager.createOWLOntologyManager();
        OWLOntologyMerger merger = new OWLOntologyMerger(inManager);

        IRI mergedOntologyIRI = IRI.create(outputDir.toUri().toString(), "credo.ttl");
        try {
            OWLOntology mergedOntology = merger.createMergedOntology(outManager, mergedOntologyIRI);
            mergedOntology.saveOntology(new TurtleDocumentFormat());
        } catch (OWLOntologyCreationException e) {
            logger.error("Failed to merge ontologies", e);
        } catch (OWLOntologyStorageException e) {
            logger.error("Failed to write out merged ontology to file", e);
        }
    }
}