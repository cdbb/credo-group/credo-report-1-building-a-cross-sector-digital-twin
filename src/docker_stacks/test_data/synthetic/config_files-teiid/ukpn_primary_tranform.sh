#!/bin/sh
# Remove any rows that don't have a value in the "Primary Substation No." column
csvgrep -l -S -c "Primary Substation No." -r ".+" < /dev/stdin |
# Remove the "m" unit from the easting and northing values
sed 's/ m//g'
