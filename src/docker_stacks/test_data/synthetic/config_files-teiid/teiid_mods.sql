-- Wrap the ST_TRANSFORM function to force Teiid to push it down to Postgres as the Teiid implementation seem to be broken
CREATE OR REPLACE FUNCTION my_transform(geom geometry, srid integer) RETURNS geometry AS $$
    SELECT ST_TRANSFORM( geom, srid );
$$ LANGUAGE SQL;
-- Increase the maximum number of connections as Teiid runs many small queries
ALTER SYSTEM SET max_connections = 500;