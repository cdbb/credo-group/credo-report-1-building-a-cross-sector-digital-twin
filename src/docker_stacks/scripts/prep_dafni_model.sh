#!/bin/bash

# Build an image, tarball it and copy to /build/dafni_models/<image_name>/version with the model definition.


if [ "$#" -lt 1 ]; then
  echo "============================================================================="
  echo " Usage:"
  echo "       $0 [image_name] <-b/--builder builder_name> <-m/--model_dir model_directory> <-f/--dockerfile dockerfilename>"
  echo "  e.g. $0 kg_data_extractor -b podman"
  echo ""
  echo " Required args:"
  echo "        image_name : name of a directory in ../ containing a Dockerfile and a model-def.yml"
  echo ""
  echo " Optional args:"
  echo "      builder_name : 'podman' or 'docker'; default is docker"
  echo "   model_directory : absolute or relative path to the model directory (default is ../image_name)"
  echo "    dockerfilename : filename of the Dockerfile"
  echo "============================================================================="
  exit 1
fi

# Process required args
image_base="$1"
shift

# Process optional args
image_builder="docker"
while test $# -gt 0; do
  case "$1" in
    --builder) ;&
    -b)
      shift
      image_builder=$1
      shift
      ;;
      --model_dir) ;&
      -m)
      shift
      src_dir=$1
      shift
      ;;
	  --dockerfile) ;&
	  -f)
	  shift
	  dockerfilename=$1
	  shift
	  ;;
    *)
      echo "Ignoring unrecognised argument [$1]"
      shift
      ;;
  esac
done

build_opts=""
case "$image_builder" in
  docker)
    ;;
  podman)
    build_opts="--security-opt label=disable"
    ;;
  *)
  echo "$image_builder must be 'docker' or 'podman'"
  exit 4
  ;;
esac

# Set directory and file locations relative to this script
script_dir="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
if [ -z $src_dir ]; then
  # If source dir wasn't provided as an argument, assume it's ../$image_base
  src_dir="$script_dir/../$image_base"
fi
date_str=$(date +%Y-%m-%d_%H-%M-%S)
dest_dir="$script_dir/../../../build/dafni_models/$image_base/$date_str"
tar_gz_file="$dest_dir/$image_base.tar.gz"

if [ -z $dockerfilename ]; then
  # If dockerfilename wasn't provided as an argument, assume it's Dockerfile
  dockerfilename="Dockerfile"
fi

docker_file="$src_dir/$dockerfilename"

# Bail out if the Dockerfile doesn't exist
if [ ! -f "$docker_file" ]; then
  echo "No Dockerfile found at $docker_file"
  exit 2
fi

# Set image tag
registry_address="dafni-models"
version="dafni-version"
tag="$registry_address/$image_base:$version"

# Build image
build_cmd="$image_builder build -f $docker_file -t $tag $build_opts $src_dir"
printf "Building image with:\n $build_cmd\n"
$build_cmd

# Copy image and model definition to dafni models dir
mkdir -p "$dest_dir"
if [ -f "$tar_gz_file" ]; then rm "$tar_gz_file"; fi
$image_builder save "$tag" | gzip > "$tar_gz_file"
\cp $src_dir/*.yml "$dest_dir"
printf "Tarballed image and model definition copied to:\n $dest_dir\n"