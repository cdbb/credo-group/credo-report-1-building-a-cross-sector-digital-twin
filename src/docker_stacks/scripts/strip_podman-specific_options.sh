#!/bin/bash

# Generate yml files that will work with docker-compose by stripping podman-specific
# options from the *.yml files in ../ (only "network_name" for now).
# The new filenames end in '.dc.yml' .

script_dir="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Remove previous versions
shopt -s nullglob
find ${script_dir}/.. -name "docker-compose.*.dc.yml" -exec rm {} \;

for p in ${script_dir}/../docker-compose.*.yml; do
  p_new=$(echo $p | sed "s/.yml/.dc.yml/")
  cat $p | grep -v network_name -v | sed "s/.yml/.dc.yml/" >$p_new
done
