## About

A minimal Argo example that demonstrates writing to, and reading from, a knowledge graph. The workflow

1. spins up a Blazegraph container in daemon mode
2. runs a 'data-uploader' container to add the contents of an RDF file to Blazegraph
3. runs a 'data-downloader' container to query Blazegraph, then convert the output to csv

## To Run in Argo locally

1. Build the blazegraph, data-uploader and data-downloader images locally (e.g. docker-compose build in this directory).
2. Install Kubernetes (In Windows, this just amounts to enabling it in Docker Desktop)
3. Install Argo and connect to the GUI [(see these instructions)](https://argoproj.github.io/argo-workflows/quick-start/)
4. Under Workflow templates, click "Create New Workflow Template", choose "Upload from file" and select "argo-workflow.yml.
5. Click "+Create", then "+Submit".
6. When the workflow finishes, click on the data-downloader container and choose "Inputs/Outputs" to see the SPARQL query results in CSV format.
