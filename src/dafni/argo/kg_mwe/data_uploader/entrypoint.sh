#!/bin/sh

# Wait for Blazegraph to come up; only needed when running with docker-compose/podman-compose
#sleep 5

cd /data/inputs/rdf
curl -X POST -H "Content-Type:application/rdf+xml" --data-binary "@input_data.rdf" "$KG_ENDPOINT"