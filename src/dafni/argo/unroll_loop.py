from asyncore import loop
import copy
import os.path
import re
import ruamel.yaml as yaml

#==================================================================================================
def add_iter_prefix(base,prefix):
    # Make a deep copy so that we don't modify the base task/template
    prefixed = copy.deepcopy(base)
    for field in ["name", "template"]:
        if field in prefixed:
            prefixed[field] = "%s-%s" % (prefix,prefixed[field])
    if "arguments" in prefixed:
        if "artifacts" in prefixed["arguments"]:
            for artifact in prefixed["arguments"]["artifacts"]:
                re_match = re.search('{{steps\.(.*?)\..*}}',artifact["from"])
                if re_match:
                    old_stepname = re_match.group(1)
                    new_stepname = prefix+"-"+old_stepname
                    artifact["from"] = artifact["from"].replace("steps.","tasks.").replace(old_stepname,new_stepname)
    return prefixed
#==================================================================================================

#==================================================================================================
def add_task_dependency(target_task, dependent_on_task):
    target_task["dependencies"] = target_task.pop("dependencies",[])
    target_task["dependencies"].append(dependent_on_task["name"])
#==================================================================================================

#==================================================================================================
def get_dag_loop_task(yml):
    loop_tasks = [task for task in get_dag_tasks(yml) if "withParam" in task]
    Nloops = len(loop_tasks)
    if Nloops == 1:
        return loop_tasks[0]
    else:
        exit("get_dag_loop_task: expected exactly one loop in dag template, but found %d" % Nloops)
#==================================================================================================

#==================================================================================================
def get_dag_tasks(yml):
    return get_dag_template(yml)["tasks"]
#==================================================================================================

#==================================================================================================
def get_dag_template(yml):
    dag_templates = [t for t in yml["spec"]["templates"] if "dag" in t ]
    Nmatch=len(dag_templates)
    if Nmatch == 1:
        return dag_templates[0]["dag"]
    else:
        exit("get_dag_template: expected exactly one dag template, but found %d" % Nmatch)
#==================================================================================================

#==================================================================================================
def get_iter_props(start_step,end_step,step_stride):
    try:
        steps = range(start_step,int(end_step)+step_stride,step_stride)
        iter_names = [ "iter%d" % iter for iter in steps ]
        iter_str = ",".join([str(num) for num in steps])
        return iter_names,iter_str
    except Exception as ex:
        print("Failed to create iter names and iter string")
        raise ex
#==================================================================================================

#==================================================================================================
def get_template_by_name(yml,name,exact=True):
    templates = get_templates(yml)
    if exact:
        template_matches = [ template for template in templates if template["name"]==name]
    else:
        template_matches = [ template for template in templates if name in template["name"]]

    Nmatch=len(template_matches)
    if Nmatch == 1:
        return template_matches[0]
    elif Nmatch==0:
        exit("No template found with name [%s]" % name)
    else:
        exit("Multiple templates found with name [%s]" % name)
#==================================================================================================

#==================================================================================================
def get_template_instances_recursive(node):
    instances=[]
    if isinstance(node,list):
        for el in node:
            instances.extend(get_template_instances_recursive(el))
    elif isinstance(node,dict):
        if "template" in node:
            instances.append(node)
        else:
            for v in node.values():
                instances.extend(get_template_instances_recursive(v))
    return instances
#==================================================================================================

#==================================================================================================
def get_templates(yml):
    try:
        return yml["spec"]["templates"]
    except KeyError as e:
        print("get_templates: Failed to find spec/templates node")
        raise(e)
#==================================================================================================

#==================================================================================================
def load(fpath):
    with open(fpath) as fh:
        yml = yaml.round_trip_load(fh, preserve_quotes=True)
    return yml
#==================================================================================================

#==================================================================================================
def print_usage(script_name):
    print("usage: python %s [workflow_subdirectory] ([first_iter],[last_iter],[n_iters])" % script_name)
    print("         where: first_iter, last_iter and n_iters are valid integers")
    print("           and: last_iter>=first_iter")
#==================================================================================================

#==================================================================================================
def rename_templates(yml):
    # For all template instances whose names differ from their template names
    templates_to_remove=[]
    for i in get_template_instances_recursive(yml):
        if i['template'] != i['name']:
            old_template = get_template_by_name(yml,i['template'])
            new_template = copy.deepcopy(old_template)
            new_template["name"] = i['name']
            i['template'] = i['name']
            yml["spec"]["templates"].append(new_template)
            templates_to_remove.append(old_template)

    # This should remove the existing templates that have been duplicated, but it doesn't seem to be necessary
    # Maybe the deep-copy isn't working as it should? It doesn't *seem* to have any negative consequences though    
    # for t in templates_to_remove:
    #     try:
    #         (yml["spec"]["templates"]).remove(t)
    #     except Exception as e:
    #         print("Warning: Failed to remove template %s" % t["name"])
#==================================================================================================

#==================================================================================================
def save(yml,fpath):
    with open(fpath,"w") as fh:
        yaml.round_trip_dump(yml,fh,width=1e9)
        print("Wrote unrolled workflow to %s" % fpath)
#==================================================================================================

#==================================================================================================
def unroll(yml_in,iter_props_str,iter_str_param="simulation-times"):

    # Extract iteration properties from iter_props_str
    # Set up iteration names and determine the value to set for <iter_str_param>
    try:
        first_iter,last_iter,iter_stride=(int(s) for s in iter_props_str.split(","))
        iter_names,iter_str = get_iter_props(first_iter,last_iter,iter_stride)
    except Exception as e:
        print("Failed to extract iteration properties from [%s]" % iter_props_str)
        print(e)

    print("Unrolling workflow loop. First_iter=%d, last_iter=%d, iter_stride=%d" % (first_iter,last_iter,iter_stride))

    yml = copy.deepcopy(yml_in)

    # Guarantee no duplicates by renaming templates to task names
    rename_templates(yml)

    templates = get_templates(yml)#####

    loop_task = get_dag_loop_task(yml)
    loop_template = get_template_by_name(yml,loop_task["template"])

    # Get tasks and templates for the loop steps. Each step can be a list itself, so unravel both layers
    step_tasks = [t for tl in loop_template["steps"] for t in tl]
    step_templates = [get_template_by_name(yml,task["template"]) for task in step_tasks]

    # First, translate loop-internal params to loop-external ones for the original set of step tasks
    param_map = {"{{inputs.parameters."+p['name']+"}}":p['value'] for p in loop_task["arguments"]["parameters"] }
    for task in step_tasks:
        if "arguments" in task:
            args = task["arguments"]
            if "parameters" in args:
                params = args["parameters"]
                for param in params:
                    if param["value"] in param_map:
                        param["value"] = param_map[param["value"]]

    templates = get_templates(yml)
    
    # Create new tasks and templates for each iteration
    new_tasks=[]
    new_templates=[]
    for iter_name in iter_names:
        iter_templates = [add_iter_prefix(template,iter_name) for template in step_templates]
        iter_tasks = [add_iter_prefix(task,iter_name) for task in step_tasks]

        # Set dependencies so as to link consecutive tasks within this iter
        for idx in range(1,len(iter_tasks)):
            add_task_dependency(iter_tasks[idx],iter_tasks[idx-1])
        # Make first task for this iter depend on last task from previous iter
        if len(new_tasks)>0:
            add_task_dependency(iter_tasks[0],new_tasks[-1])

        new_tasks.extend(iter_tasks)
        new_templates.extend(iter_templates)
    
    # Get current tasks and templates
    dag_tasks = get_dag_tasks(yml)
    templates = get_templates(yml)

    # Any tasks that depended on the loop should now depend on the final new_task
    for t in dag_tasks:
        if "dependencies" in t:
            if loop_task["name"] in t["dependencies"]:
                t["dependencies"].remove(loop_task["name"])
                t["dependencies"].append(new_tasks[-1]["name"])

    # Any tasks which the loop depended on should now be dependencies of the first new_task
    if "dependencies" in loop_task:
        new_tasks[0]["dependencies"] = new_tasks[0].pop("dependencies",[])
        new_tasks[0]["dependencies"].extend(loop_task["dependencies"])

    # Record the loop's position in the task list
    loop_task_idx = dag_tasks.index(loop_task)

    # Remove the loop task and template
    dag_tasks.remove(loop_task)
    templates.remove(loop_template)

    # Remove templates for the original step tasks ('step_templates' contains modified versions, so get the originals by name)
    orig_step_templates = [t for t in templates if t["name"] in [t["name"] for t in step_templates]]
    for template in orig_step_templates:
        templates.remove(template)

    # Insert the new tasks where the loop used to be
    dag_tasks[loop_task_idx:loop_task_idx] = new_tasks

    # Add all of the new templates (order and position doesn't matter)
    templates.extend(new_templates)

    # If any tasks have an input parameter called <iter_str_param>, set its value to <iter_str>
    for task in dag_tasks:
        if "arguments" in task:
            args=task["arguments"]
            if "parameters" in args:
                matches = [p for p in args["parameters"] if p["name"]==iter_str_param]
                for match in matches:
                    match["value"] = iter_str
    return yml
#==================================================================================================

#==================================================================================================
def main(wf_name, iter_props_str):
    # Set some locations
    wf_dir   = os.path.join(os.path.dirname(__file__),wf_name)
    orig_wf_fpath = os.path.join(wf_dir,"argo-workflow.local.yml")
    unrolled_wf_fpath = os.path.join(wf_dir,"argo-workflow.local.unrolled.yml")

    if not os.path.isfile(orig_wf_fpath):
        exit("No workflow file found at %s" % orig_wf_fpath)

    # Load the original workflow yml
    orig_yml = load(orig_wf_fpath)

    # Unroll the loop
    dafni_wf_yml  = unroll(orig_yml, iter_props_str)

    # Save the new yml
    save(dafni_wf_yml,unrolled_wf_fpath)
#==================================================================================================

if __name__ == "__main__":
    import sys
    Nargs = len(sys.argv)-1
    if Nargs==2:
        main(sys.argv[1], sys.argv[2])
    else:
        print_usage(sys.argv[0])