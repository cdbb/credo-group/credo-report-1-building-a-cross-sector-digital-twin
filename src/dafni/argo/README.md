# Running the workflow locally
## Prerequisites

The following instructions assume that you're working on Windows and already have [Docker Desktop](https://www.docker.com/products/docker-desktop) installed.  They also assume that you have pulled this repository in WSL and are running scripts from there, rather than in cmd or Powershell on Windows itself.

1. **Install/Enable Kubernetes**:
  * In Docker Desktop, choose 'Settings', then 'Kubernetes'
  * Tick 'Enable Kubernetes' and wait for the components to be installed.
2. **Install Argo**:
  * The ['quick-start' instructions](https://argoproj.github.io/argo-workflows/quick-start/) seem to be sufficient for almost all local testing, but it's also possible to install [particular versions](https://github.com/argoproj/argo-workflows/releases).
  * For the quick-start approach, run the following in a cmd window:
    ```
    kubectl create ns argo
    kubectl apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/master/manifests/quick-start-postgres.yaml
    ```
3. **Edit your hosts file**:
  * To allow Kubernetes to use images built with 'localhost' as the image registry, you need to edit the Windows host file at C:\Windows\System32\drivers\etc\hosts
  * Run an editor (e.g. Notepad++) as an administrator, then add the following at the end of the file:
    ```
    127.0.0.1 view-localhost
    ```

> If Avira Antivirus blocks you from editing the hosts file, open Avira, then:
> * Choose the "Security" tab
> * Click the faint grey cog symbol next to "Real-time protection"
> * Expand the "General" node on the left, then choose "Security" and uncheck "Protect Windows hosts files from changes"
## Setting up input data and preparing images

Note that, when generating images for local Argo testing, input files are built into the images themselves.  The data files must be stored in a subdirectory of `src/docker_stacks/test_data`.

1. **Add flood data**:
  * Flood data files aren't committed to the repository. Copy flood data into your chosen test dataset at `src/docker_stacks/test_data/<dataset_name>/flood_data`.

2. **Build images for local testing**:
  * From `src/docker_stacks`, run
  ```
    ARGO_TEST_DSET_NAME=<dataset_name> docker-compose -f docker-compose.argo.yml build
  ```
  N.B. If there are podman-specific commands in the compose file, you'll need to strip them first. Instead of the above, run:
  ```
  ./scripts/strip_podman-specific_options.sh
  ARGO_TEST_DSET_NAME=<dataset_name> docker-compose -f docker-compose.argo.dc.yml build
  ```

3. **Modify timesteps in the template**
  * The sub-directories next to this file each contain separate workflows. The local template file for each is called `argo-workflow.local.yml`.
  * This file needs to be modified to set the values run by the timestep loop, which should match the times associated with your flood data.
  * In the template, find the 'timestep-loop' task and change 'timestep-list-str' to a comma-separated list of values and the 'withParam' field to a list of the same values; e.g.
  ```
          - name: timestep-list-str
            value: "1,2,3"
        ...
        withParam: '[ "1" , "2" , "3" ]'
  ```

  > Note that it won't be necessary to modify the template in future - the timestep list will be set as a workflow parameter at runtime.

## Start the workflow

1. **Connect to the Argo GUI**:
  * Run this in cmd to forward a local port to the pod running the Argo GUI:
    ```
    kubectl -n argo port-forward deployment/argo-server 2746:2746
    ```
  * Connect your browser to localhost:2746 and accept the self-signed certificate.
2. **Load the local template**
  * Go to the "Workflow Template" page (second icon down) and click "Create New Workflow Template".
  * Choose "Upload File", select the local version of the template (e.g. `./full/argo-workflow.local.yml`) and click "Create".
3. **Submit the workflow**
  * Choose "Submit" then use the panel on the right to set workflow-level parameters.
4. **View logs and output data**
  * As the workflow runs, click on a container and then "Logs" at the top of the panel to see live output from stdout.
  * Click on a container then "Inputs/Outputs" in the right-hand pane to see the input parameters that were set and the contents of any artifacts.

---

# Running the workflow on DAFNI

For the workflow to run on DAFNI,

1. all images referenced in the workflow must have been added to the DAFNI model store (the National Infrastructure Modelling Service; NIMS).
1. all *input* datasets used in the workflow must have been added to the DAFNI data store (the National Infrastructure Database; NID).
1. the local template must have been transformed to accommodate the way Argo is used on DAFNI.

## Uploading new/updated models

Each model should reside in a separate sub-directory of `src/docker_stacks`.
That directory should include a Dockerfile and any source code required to build the image.
Note that some directories also include Dockerfile.argo; these are used *only when running the workflow locally* and are needed because they build-in input data that would be mounted from NID when running on DAFNI.
> N.B. This two-Dockerfile approach could be avoided using build targets, provided the compose files for each stack and the model preparation script are modified accordingly).

To upload a new model to NIMS, or update an existing one:

1. **Add/update model-def.yml**: See existing model definition files for guidance, as well as the [basic instructions](https://docs.secure.dafni.rl.ac.uk/docs/how-to/how-to-write-a-model-definition-file) on DAFNI and the [detailed list of possible model fields](https://docs.secure.dafni.rl.ac.uk/docs/reference/model-definition-reference).
2. **Run the model preparation script**:

  * From `src/docker_stacks`, run
    ```
    ./scripts/prep_dafni_model.sh [model_sub-directory]
    ```
  * If the model directory isn't in src/docker_stacks, run
  ```
  ./scripts/prep_dafni_model.sh [model_name] [-m path_to_model_directory]
  ```
  * The model definition and tarballed image are written to `build/dafni_models/[model_name]/[current_datetime]`
  * Ensure that [model_name] does not have a trailing slash, e.g. 'blazegraph/'

3. **Add/modify model files on DAFNI**:
  * To add a new model, go to https://facility.secure.dafni.rl.ac.uk/models/add/ and choose the files generated above.
  * To update a model, go to https://facility.secure.dafni.rl.ac.uk/models, select a model, then click 'Edit model' and choose the files generated above.

4. **Note the model ID**: Select the model, verify that the metadata is correct, and note the 'ID' assigned by DAFNI (not the 'Parent ID')

Troubleshooting - when uploading several models in a row, very frequently only the first one works correctly.
Opening the page in a new browser window seems to fix the problem.

## Uploading new data

1. To update an existing dataset go to https://facility.secure.dafni.rl.ac.uk/data, then
  * Search the catalogue and click on the target dataset.
  * Click 'Edit dataset', choose your files and update the metadata if necessary, then click "Publish new version".
  * Click back to the dataset and note the "Dataset Version ID".
1. To add an entirely new dataset, go to https://facility.secure.dafni.rl.ac.uk/data/add/, then
  * Choose your files, fill in the metadata values and click "Publish".
  * Click on the new entry created in the Data Catalogue and note the "Dataset Version ID".

> Caveat: At the time of writing, the data uploader tool can't handle datasets with sub-directories; files are stored in a flat list on NID.  Your only option is to zip the data, upload it, then unzip it again *inside the model*.  **However**, if a dataset is written by a publishing step in an Argo/DAFNI GUI workflow, rather than being uploaded directly, it **can** have subdirectories. The directory structure is then recreated automatically when the dataset is used in a model - it doesn't need to be handled by the model itself.


## The transformation script

Transforming the local workflow template for use on DAFNI entails removing some Argo features that aren't supported by DAFNI's backend interface and replacing them with alternative solutions, then adding components that fetch images from the DAFNI model store and read/write data from/to the DAFNI datastore.  The script uses the 'ruamel' Python library to modify and augment the local yaml template file. The documentation for ruamel is not great, but it has a lot of useful features that Python's built-in yaml library doesn't.

The script requires two configuration files:
* `dafni_options.yml` - the main config file for each workflow, committed to the repository; described in the next subsection.
* `dafni_settings_private.yml` - a config file applied to *all* workflows. It contains fields that shouldn't be committed to the repository, e.g. Keycloak credentials.
  These details can be set by the DAFNI admins easily enough, so all of the fields can be left blank if necessary. If you don't have one in the workflow already, copy the template from this directory:
```
cd src/dafni/argo
cp dafni_settings_private.template.yml dafni_settings_private.yml
```

### Setting transformation options

Options for the script are set using `dafni_options.yml`.

* The **data_retriever** node specifies which data should be fetched from NID, and which containers should have access to it.
  **data_retriever/tasks**' is a yaml list, with each entry containing one dset_ID (the "Dataset Version ID" described in the "Uploading new data" section above ) and a list of one or more paths at which to mount the data in the shared data volume used by every DAFNI workflow.
  This data is passed on to each container by mounting `/data/<template_name>` in the shared volume, to `/data` in the container itself.
  Hence, in the example below, the first dataset will be available in the 'pgsql-uploader' container at /data/inputs/assets, while the second will be available to the 'pgsql-uploader' and 'postgis' containers at `/data/inputs/all/flood_data` and the 'vis-data-extractor' container at `/data/inputs/geotif-data/flood`.
```
data_retriever:
    tasks:
        - dset_ID: 3bbd3e49-7437-4ec1-b6b6-0f531e106e91
          input_dirs: [ "/data/pgsql-uploader/inputs/assets" ]
        - dset_ID: d68584e7-7230-4c7b-b759-8837c289d716
          input_dirs: [ "/data/pgsql-uploader/inputs/all/flood_data","/data/postgis/inputs/all/flood_data","/data/vis-data-extractor/inputs/geotif-data/flood" ]
```

* The **images** node is a yaml list describing how local image tags should be mapped to DAFNI image tags.  For example, the list entry below will translate `localhost/blazegraph:1.0.0` to `<NIMS user registry>/blazegraph:<model_ID>`, where `localhost` and `<NIMS user registry>` are hard-coded in the transformation script. 'local_name' and 'local_version' must match the tag used in src/docker-stacks/docker-compose.argo.yml and dafni_name must match the value of the **metadata/name** node in the model-definition file used when uploading the image.
```
images:
    - local_name: blazegraph
      local_version: 1.0.0
      dafni_name: blazegraph
      dafni_version: 4fadb4c5-0571-48b5-a40d-7dfe6f971a23

```

* The **publisher_opts** node contains **dset_opts**, which specifies the metadata to attach to the dataset published at the end of the workflow and **files**, which determines what get published. Filepaths are relative to /data inside the named task/container, so the example below publishes everything in /data/outputs inside the 'vis-data-extractor' container.
```
publisher_opts:
    dset_opts:
        creation_date: "2022-01-20T12:00:00Z"
        desc: "Output data from testing of the CReDO workflow (full version)."
        extra:
            geojson: {}
        keywords:  [ "credo", "test" ]
        subject: "Climatology / Meteorology / Atmosphere"
        title: CReDO workflow output (testing)
        version_desc: "Initial Dataset version"
    files:
        - task: vis-data-extractor
          path_patterns: [ "outputs/**/*" ]
```

* The **shared_vol** node define the name and total amount of storage allocated to the shared volume used by all workflow containers. These two properties should rarely need to change.  **shared_vol/subpaths** defines which subpaths within the shared data volume that each container has access to.  'null' means no subpath, i.e. the container has access to all of /data,  rather than /data/template_name.
> Suggested improvement: The subpaths node shouldn't be required, the script should just pick up which containers have input and/or output artifacts and give them access to a subpath matching their template name. 
```
shared_vol:
    name: shared-workflow-volume
    storage: 128Gi
    subpaths:
        dataretrieval: null
        kg-initialiser: kg-initialiser
        ontop: ontop
        pgsql-uploader: pgsql-uploader
        postgis: postgis
        directory-structure-creator: null
        publisher: null
        vis-data-extractor: vis-data-extractor
```

* Finally, the **workflow** node sets a label for the top-level metadata and a workflow name, which is used to tag all containers and the published dataset.
Functionally, it doesn't matter which values are chosen for these fields.
```
workflow:
    label: credo-workflow-full
    name: credo-workflow-full
```

### Running the transformation

Before running the script:

* The workflow directory must be next to this readme. The required directory structure looks like this:
```
src/dafni/argo/
  <workflow_directory>/
    argo-workflow.local.yml
    dafni_options.yml
  dafni_settings_private.yml
```

* If `dafni_settings_private.yml` doesn't already exist, copy it from the template as described at the beginning of this section.

* If `dafni_options.yml` doesn't already exist, copy it from (e.g.) `./full` and modify as needed.


To run the script:

1. Create a Python virtual environment (only needed on first run)
```
venv=<location at which to create venv>
mkdir -p $venv && python3 -m venv $venv
```

2. Activate the environment
```
. $venv/bin/activate
```

3. Install requirements (only needed on first run)
```
pip install -r requirements.txt
```

4. Run the transformation script
```
python dafnify_argo_workflow.py [workflow_name]
```
This generates `<workflow_name>/argo-workflow.dafni.yml`, which can be sent to DAFNI admins to be executed on their Argo server.

### 'Unrolling' the timestep loop

At time of writing, the Argo features required to run timestep loop iterations sequentially are not available on DAFNI.
For this reason, a second transformation script, `unroll_loop.py`, is provided to extract loop steps and add them as dag tasks instead, replicating each step once per iteration.

The script:
1. Accepts user-specifed values for the first iteration, last iteration and iteration 'stride'.
1. Duplicates loop steps accordingly, handling dag dependencies between adjacent steps and with other dag tasks.
1. Converts loop-internal variables so that each model works outside the loop.
1. Duplicates templates as necessary to ensure there is exactly one dag task per template (DAFNI needs this so data can be shared between containers correctly using volume mounts).

The main way to use the script is indirectly, by passing additional arguments to `dafnify_argo_workflow.py` (you'll need to have already run steps 1-3 in the previous subsection):
```
cd src/dafni/argo
python dafnify_argo_workflow.py [workflow_name] [-u first_iter,last_iter,iter_stride]
```
which generates `argo-workflow.dafni.yml` with all the normal DAFNI options added, but with the loop unrolled.
For example, if you were running the 'full' workflow, with flood data outputs separated by one hour (3600 s) starting at t=21600 s and ending at t=93600 s, and files named accordingly, you would run:
```
python dafnify_argo_workflow.py full -u 21600,93600,3600
```

For local testing purposes, the unroll script can also be run on its own, without adding DAFNI options:
```
cd src/dafni/argo
python unroll_loop.py [workflow_name] [first_iter,last_iter,iter_stride]
```
which generates `argo-workflow.local.unrolled.yml`.
