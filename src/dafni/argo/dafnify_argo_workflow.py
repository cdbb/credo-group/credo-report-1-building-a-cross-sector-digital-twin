import collections.abc
import copy
import os.path
from time import daylight
import ruamel.yaml as yaml
from ruamel.yaml.comments import CommentedMap,CommentedSeq

from unroll_loop import unroll as load_unrolled

quotedStr = yaml.scalarstring.DoubleQuotedScalarString
singleQuotedStr = yaml.scalarstring.SingleQuotedScalarString
NULL=None

# Some constants used to set task/template names consistently
DIR_CREATOR_TASK_NAME = "directory-structure-creator"
DIR_CREATOR_TEMPLATE_NAME = "directory-structure-creator"
PUBLISHER_TASK_NAME = "publish"
PUBLISHER_TEMPLATE_NAME = "publisher"
RETRIEVER_TASK_NAME_PREFIX = "dataretrieval-"
RETRIEVER_TEMPLATE_NAME = "dataretrieval"

#================== Helper functions for creating and manipulating ruamel objects =================
def flowStyleDict(d):
    ds = CommentedMap(d)
    ds.fa.set_flow_style()
    return ds

def flowStyleList(l):
    cs = CommentedSeq(l)
    cs.fa.set_flow_style()
    return cs

def quote_dict_keyvals_rec(obj):
    if type(obj)==dict:
        qd = dict(obj)
        for key in list(qd.keys()):
            qkey = quotedStr(key)
            val = qd.pop(key)
            qd[qkey] = quote_dict_keyvals_rec(val)
        return qd
    elif type(obj)==list:
        return [quote_dict_keyvals_rec(el) for el in obj]
    elif type(obj)==tuple:
        return tuple(quote_dict_keyvals_rec(el) for el in obj)
    elif type(obj)==str:
        return quotedStr(obj)
    else:
        return obj
#==================================================================================================

#==================================================================================================
def add_common_container_opts(yml,config):
    for template in get_container_template_nodes(yml):
        template["metadata"] = get_default_container_metadata(template["name"],config)
        # N.B. Need to copy common_container_opts with dict(), otherwise ruamel writes a reference instead of the object
        template["container"].update(dict(imagePullPolicy="Always",resources=get_default_container_resources()))
#==================================================================================================

#==================================================================================================
def add_data_retriever_task(yml, retriever_opts, idx=None):
    # Add dag task
    data_retrieval_node=dict(name=RETRIEVER_TASK_NAME_PREFIX+retriever_opts["dset_ID"], template=RETRIEVER_TEMPLATE_NAME, dependencies=[] ,arguments={})    
    data_retrieval_node["dependencies"] = flowStyleList([quotedStr(DIR_CREATOR_TASK_NAME)])
    data_retrieval_node_params = [dict(name="dataset-version-uid",value=retriever_opts["dset_ID"])]
    vol_paths=flowStyleList([quotedStr(p) for p in retriever_opts["input_dirs"]])
    data_retrieval_node_params.append(dict(name="volume-path",value=vol_paths))
    data_retrieval_node["arguments"]["parameters"] = data_retrieval_node_params
    insert_idx = idx if idx is not None else 0
    yml["spec"]["templates"][0]["dag"]["tasks"].insert(insert_idx,data_retrieval_node)
#==================================================================================================

#==================================================================================================
def add_data_retriever_template(yml, cred_opts, idx=None):
    # Add template
    args = [quotedStr('{{inputs.parameters.dataset-version-uid}}'),quotedStr('{{inputs.parameters.volume-path}}')]
    command = flowStyleList([quotedStr('python'),quotedStr('dataretrieval.py')])
    container=dict(image=make_image_tag("management-container","latest",internal_reg=True),command=command,args=args,env=get_nid_env(cred_opts))
    inputs=dict(parameters=[dict(name="dataset-version-uid"),dict(name="volume-path")])
    template=dict(name=RETRIEVER_TEMPLATE_NAME,container=container,inputs=inputs)
    add_template(yml,template,idx)
#==================================================================================================

#==================================================================================================
def add_data_transfer_template(yml):
    args = [singleQuotedStr("{{inputs.parameters.from_step}}"), singleQuotedStr("{{inputs.parameters.to_step}}")]
    command = ["python", "step_data_transfer.py"]
    env = [dict(name="VOLUME_MOUNT_PATH",value="/data/")]
    image="reg.dafni.rl.ac.uk/dafni/nims/management-container/staging"
    container = dict(args=args,command=command,env=env,image=image)
    container["volumeMounts"] = [dict(mountPath="/data/",name="shared-workflow-volume")]
    inputs=dict(parameters=[dict(name="from_step"),dict(name="to_step")])
    template = dict(name='data-transfer-step',container=container,inputs=inputs)

    add_template(yml,template)    
#==================================================================================================

#==================================================================================================
def add_data_transfer_task(yml,transfer):
    from_sub_dir = '/'.join(transfer["from_path"].split('/')[3:])
    to_sub_dir = '/'.join(transfer["to_path"].split('/')[3:])
    if (from_sub_dir != to_sub_dir):
        err_msg = " To replace an artifact with a data transfer tasks requires the output subdirectory in the source task to match the input subdirectory in the destination task.\n"
        err_msg += "   Current subdirs are %s (%s outputs) and %s (%s inputs)" % (from_sub_dir,transfer["from_task"],to_sub_dir,transfer["to_task"])
        exit(err_msg)
    else:
        task_name="%s-step-data-transfer-%s" % (transfer["from_task"],transfer["to_task"])
        params = [dict(name="from_step",value=transfer["from_task"]),dict(name="to_step",value=transfer["to_task"])]
        task=dict(name=task_name,template="data-transfer-step",arguments=dict(parameters=params),dependencies=[quotedStr(transfer["from_task"])])

        dag_tasks = get_dag_tasks(yml)
        from_task = match_node_name_unique(dag_tasks,transfer["from_task"])
        from_task_idx=dag_tasks.index(from_task)
        dag_tasks.insert(from_task_idx+1,task)

        # Modify to_task's dependencies by adding the transfer task, and removing anything the transfer task itself depends on
        to_task = match_node_name_unique(dag_tasks,transfer["to_task"])
        to_tasks_dependencies = [dep for dep in to_task.pop("dependencies",[]) if dep not in task["dependencies"] ]
        to_tasks_dependencies.append(quotedStr(task_name))
        to_task["dependencies"] = flowStyleList(to_tasks_dependencies)
#==================================================================================================

#==================================================================================================
def add_dir_struct_creator_task(yml,idx=None):
    # Add dag task
    dir_structure_node=dict(name=DIR_CREATOR_TASK_NAME, template=DIR_CREATOR_TEMPLATE_NAME)
    insert_idx = idx if idx is not None else 0
    yml["spec"]["templates"][0]["dag"]["tasks"].insert(insert_idx,dir_structure_node)
#==================================================================================================

#==================================================================================================
def add_dir_struct_creator_template(yml,vol_opts,idx=None):
    # Add the template
    args = [quotedStr(os.path.join('/data',template_name)) for template_name,subpath in vol_opts["subpaths"].items() if subpath is not None]
    command = flowStyleList([quotedStr('python'),quotedStr('directory_structure_creator.py')])
    container=dict(image=make_image_tag("management-container","latest",internal_reg=True),command=command,args=args)
    template=dict(name=DIR_CREATOR_TEMPLATE_NAME,container=container)
    add_template(yml,template,idx)
#==================================================================================================

#==================================================================================================
def add_publisher(yml,config,idx=None):
    # Add template
    args = get_publisher_args(config["publisher_opts"])
    command = flowStyleList([quotedStr('python'),quotedStr('datapublish.py')])
    env=[]
    env.append(make_name_val_pair("WORKFLOW_ID",config["workflow"]["name"]))
    env.append(make_name_val_pair("WORKFLOW_NAME",config["workflow"]["name"]))
    env.append(make_name_val_pair("INSTANCE_ID",config["workflow"]["name"]))
    env.extend(get_nid_env(config["credentials"],include_dss=True))
    container=dict(image=make_image_tag("management-container","latest",internal_reg=True),command=command,args=args,env=env)
    template=dict(name=PUBLISHER_TEMPLATE_NAME,container=container)
    add_template(yml,template,idx)

    # Add dag task
    publish_task_node = dict(name=PUBLISHER_TASK_NAME,template=PUBLISHER_TEMPLATE_NAME)
    yml["spec"]["templates"][0]["dag"]["tasks"].append(publish_task_node)
#==================================================================================================

#==================================================================================================
def add_shared_data_vol(yml,vol_opts):
    # Add volume mounts
    for container_name,subpath in vol_opts["subpaths"].items():
        container=get_template_container_by_name(yml,container_name)
        add_shared_vol_mount_to_container(container,vol_opts["name"],subpath=subpath)

    # Add volume claim
    vol_claim_spec=dict(accessModes=["ReadWriteMany"], resources=dict(requests={"storage":vol_opts["storage"]}), storageClassName="nfs-client")
    vol_claim=dict(metadata=dict(name=vol_opts["name"]),spec=vol_claim_spec)
    add_vol_claim(yml,vol_claim)
#==================================================================================================

#==================================================================================================
def add_shared_vol_mount_to_container(container,name,subpath=None):
    mount_props=dict(mountPath=quotedStr('/data'),name=name)
    if subpath is not None:
        mount_props["subPath"] = subpath
    container["volumeMounts"] = [mount_props]
#==================================================================================================

#==================================================================================================
def add_shared_vol_subpaths_for_transfers(config,new_template_names):
    shared_vol_opts = config["shared_vol"]
    subpaths = shared_vol_opts.pop("subpaths",{})
    for template_name in new_template_names:
        if template_name not in subpaths:
            subpaths[template_name]=template_name
    shared_vol_opts["subpaths"] = subpaths
#==================================================================================================

#==================================================================================================
def add_spec_settings(yml):
    yml["spec"]["serviceAccountName"] = "nims-workflow-production"
#==================================================================================================

#==================================================================================================
def add_template(yml,template,idx=None):
    templates = yml["spec"]["templates"]
    insert_idx = len(templates) if idx is None else idx
    templates.insert(insert_idx,template)
#==================================================================================================

#==================================================================================================
def add_top_level_meta(yml,config):
    top_level_metadata = dict(name=config["workflow"]["name"],namespace=quotedStr('dafni-nims-workflow-production'))
    top_level_metadata["labels"]={quotedStr('nims/workflowName'): config["workflow"]["label"]}
    # Overwrite any existing metadata
    yml["metadata"] = top_level_metadata
#==================================================================================================

#==================================================================================================
def add_vol_claim(yml,vol_claim,idx=None):
    vol_claims = yml["spec"].pop("volumeClaimTemplates",[])
    if idx is None:
        vol_claims.append(vol_claim)
    else:
        vol_claims.insert(idx,vol_claim)
    yml["spec"]["volumeClaimTemplates"] = vol_claims
#==================================================================================================

#==================================================================================================
def collect_transfers(yml_node):
    # N.B. Only detects transfers within yml lists (i.e. within a step or dag task)
    transfers=[]    
    if isinstance(yml_node, list):
        for to_task in yml_node:
            if isinstance(to_task, dict):
                if "arguments" in to_task:
                    to_args = to_task["arguments"]
                    if isinstance(to_args, dict) and "artifacts" in to_args:
                        # Found an artifact passed as an argument. Store details in the transfers list.
                        for artifact in to_args["artifacts"]:
                            # Use the "from" descriptor to identify the source task and artifact 
                            from_split = artifact["from"].split(".")
                            from_task_name=from_split[1]
                            from_artifact_name = (from_split[-1]).strip('}')

                            from_task = match_node_name_unique(yml_node,from_task_name)
                            transfers.append(dict(from_artifact_name=from_artifact_name,
                                                  from_params=from_task["arguments"]["parameters"],
                                                  from_task=from_task_name,
                                                  from_template=from_task["template"],
                                                  to_artifact_name=artifact['name'],
                                                  to_params=to_args["parameters"],
                                                  to_task=to_task["name"],
                                                  to_template=to_task["template"]
                                                  )
                                            )
                else:
                    for el_inner in to_task.values():
                        transfers.extend(collect_transfers(el_inner))
    elif isinstance(yml_node, dict):
        for el in yml_node.values():
            transfers.extend(collect_transfers(el))

    return transfers
#==================================================================================================

#==================================================================================================
def convert_artifacts_to_data_transfer_tasks(yml):
    transfers = collect_transfers(yml["spec"])

    # Determine the paths associated with each template
    container_templates = get_container_template_nodes(yml)
    for transfer in transfers:
        from_template = match_node_name_unique(container_templates,transfer["from_template"])
        from_artifact = match_node_name_unique(from_template["outputs"]["artifacts"],transfer["from_artifact_name"])
        to_template = match_node_name_unique(container_templates,transfer["to_template"])
        to_artifact = match_node_name_unique(to_template["inputs"]["artifacts"],transfer["to_artifact_name"])
        from_path=resolve_template_path(from_artifact["path"],transfer["from_params"])
        to_path=resolve_template_path(to_artifact["path"],transfer["to_params"])
        transfer.update({"from_path":from_path,"to_path":to_path})

    # Add data transfer template
    if len(transfers) > 0:
        add_data_transfer_template(yml)

    # Add all data transfer tasks
    for transfer in transfers:
        add_data_transfer_task(yml,transfer)

    remove_all_artifacts(yml)

    # Return a set of all template_names involved in data transfers
    template_names_with_transfers = [t["from_template"] for t in transfers]
    template_names_with_transfers.extend([t["to_template"] for t in transfers])
    return list(set(template_names_with_transfers))
#==================================================================================================

#==================================================================================================
def get_template_by_name(yml,name):
    template_matches = [ template for template in yml["spec"]["templates"] if template["name"]==name]
    Nmatch=len(template_matches)
    if Nmatch == 1:
        return template_matches[0]
    else:
        exit("Expected exactly one template with name [%s], but found %d" % (name,Nmatch))
#==================================================================================================

#==================================================================================================
def get_template_container_by_name(yml,name):
    return get_template_by_name(yml,name)["container"]
#==================================================================================================

#==================================================================================================
def get_container_nodes(yml):
    return [n["container"] for n in get_container_template_nodes(yml)]
#==================================================================================================

#==================================================================================================
def get_container_template_nodes(yml):
    return [n for n in yml["spec"]["templates"] if "container" in n]
#==================================================================================================

#==================================================================================================
def get_dag_sort_idx(task):
    # Sorts tasks such that directory creator is first, followed by dataretrieval, all other tasks, then publisher last
    idx_map={DIR_CREATOR_TASK_NAME:0,RETRIEVER_TASK_NAME_PREFIX:1,PUBLISHER_TASK_NAME:1e9}
    for prefix,idx in idx_map.items():
        if prefix in task["name"]:
            return idx
    return 2
#==================================================================================================

#==================================================================================================
def get_dag_tasks(yml):
    return get_dag_template(yml)["tasks"]
#==================================================================================================

#==================================================================================================
def get_dag_tasks_match_name(yml,sub_str,count=None,exact=True):
    if exact:
        matching_tasks = [t for t in get_dag_tasks(yml) if sub_str == t["name"] ]
    else:
        matching_tasks = [t for t in get_dag_tasks(yml) if sub_str in t["name"] ]

    if count is not None:
        Nmatch=len(matching_tasks)
        if Nmatch != count:
            exit("get_dag_tasks_match_name: expected %d task containing string '%s', but found %d" % (count,sub_str,Nmatch) )
    return matching_tasks
#==================================================================================================

#==================================================================================================
def get_dag_template(yml):
    dag_templates = [t for t in yml["spec"]["templates"] if "dag" in t ]
    Nmatch=len(dag_templates)
    if Nmatch == 1:
        return dag_templates[0]["dag"]
    else:
        exit("get_dag_template: expected exactly one dag template, but found %d" % Nmatch)
#==================================================================================================

#==================================================================================================
def get_default_container_metadata(template_name,config):
    labels = {}
    labels[quotedStr('nims/userId')]     = quotedStr(config["credentials"]["ID"])
    labels[quotedStr('nims/stepId')]     = template_name
    labels[quotedStr('nims/workflowId')] = config["workflow"]["name"]
    return dict(labels=labels)
#==================================================================================================

#==================================================================================================
def get_default_container_resources():
    default_resources_limits = dict(cpu=16,memory="128Gi")
    default_resources_requests = dict(cpu="200m",memory="128Mi")
    default_resources = dict(limits=default_resources_limits,requests=default_resources_requests)
    return default_resources
#==================================================================================================

#==================================================================================================
def get_dset_meta_instep(dset_opts):
    meta={}
    meta["@type"]                  = "dcat:Dataset"
    meta.update(dset_opts["extra"])
    meta["@context"]               = ["metadata-v1"]
    meta["dct:title"]              = dset_opts["title"]
    meta["dcat:theme"]             = []
    meta["dct:rights"]             = NULL
    meta["dct:created"]            = dset_opts["creation_date"]
    meta["dct:creator"]            = [{"@id": "https://dafni.ac.uk/", "@type": "foaf:Organization", "foaf:name": "test", "internalID": NULL}]
    meta["dct:license"]            = {"@id": "https://creativecommons.org/licences/by/4.0/", "@type": "LicenseDocument", "rdfs:label": NULL}
    meta["dct:spatial"]            = {"@id": NULL, "@type": "dct:Location", "rdfs:label": NULL}
    meta["dct:subject"]            = dset_opts["subject"]
    meta["dcat:keyword"]           = dset_opts["keywords"]
    meta["dct:language"]           = "en"
    meta["dct:publisher"]          = {"@id": NULL, "@type": "foaf:Organization", "foaf:name": NULL, "internalID": NULL}
    meta["dct:conformsTo"]         = {"@id": NULL, "@type": "dct:Standard", "label": NULL}
    meta["dct:identifier"]         = []
    meta["dct:description"]        = dset_opts["desc"]
    meta["dct:PeriodOfTime"]       = {"type": "dct:PeriodOfTime", "time:hasEnd": NULL, "time:hasBeginning": NULL}
    meta["dcat:contactPoint"]      = {"@type": "vcard:Organization", "vcard:fn": dset_opts["contact"]["fn"], "vcard:hasEmail": dset_opts["contact"]["email"]}
    meta["dafni_version_note"]     = dset_opts["version_desc"]
    meta["dct:accrualPeriodicity"] = NULL

    # Quote everything
    meta = quote_dict_keyvals_rec(meta)
    return meta
#==================================================================================================

#==================================================================================================
def get_nid_env(cred_opts,include_dss=False):
    env = []
    env.append(make_name_val_pair("NID_URL","http://production-auto-deploy.dafni-nid-api.svc.cluster.local:5000/nid"))
    env.append(make_name_val_pair("VOLUME_MOUNT_PATH","/data/"))
    env.append(make_name_val_pair("USER_TOKEN",cred_opts["ID"]))
    if include_dss:
        env.append(make_name_val_pair("DSS_URL","http://production-auto-deploy.dafni-dss-dssauth.svc.cluster.local:5000"))
    env.append(make_name_val_pair("KEYCLOAK_ENDPOINT",cred_opts["keycloak"]["endpoint"]))
    env.append(make_name_val_pair("KEYCLOAK_REALM",cred_opts["keycloak"]["realm"]))
    env.append(make_name_val_pair("KEYCLOAK_CLIENT",cred_opts["keycloak"]["client"]))
    env.append(make_name_val_pair("KEYCLOAK_CLIENT_SECRET",cred_opts["keycloak"]["client_secret"]))
    env.append(make_name_val_pair("KEYCLOAK_USERNAME",cred_opts["keycloak"]["username"]))
    env.append(make_name_val_pair("KEYCLOAK_PASSWORD",cred_opts["keycloak"]["password"]))
    return env
#==================================================================================================

#==================================================================================================
def get_publisher_args(publisher_opts):
    publish_args = {}
    publish_args[quotedStr('kind')]=quotedStr('publisher')
    publish_args[quotedStr('name')]=quotedStr('publish')

    filelist=[]
    for file in publisher_opts["files"]:
        filelist.append(flowStyleDict({quotedStr('step'):quotedStr(file['task']),quotedStr('paths'):[quotedStr(el) for el in file['path_patterns']]}))
    publish_args[quotedStr('files')]=flowStyleList(filelist)
    dset_meta_instep = get_dset_meta_instep(publisher_opts["dset_opts"])
    publish_args[quotedStr('metadata')] = {quotedStr('in_step'):dset_meta_instep}
    return flowStyleList([quotedStr(PUBLISHER_TASK_NAME),publish_args])
#==================================================================================================

#==================================================================================================
def load(fpath):
    with open(fpath) as fh:
        yml = yaml.round_trip_load(fh, preserve_quotes=True)
    return yml
#==================================================================================================

#==================================================================================================
def make_image_tag(name,version,internal_reg=False):
    # DAFNI internal registry includes 'built-in' containers like the data retriever, directory structure creator, publisher
    if internal_reg:
        registry="reg.dafni.rl.ac.uk/dafni/nims"
    else:
        registry="dreg.platform.dafni.rl.ac.uk/nims-prod"
    return "%s/%s:%s" % (registry,name,version)
#==================================================================================================

#==================================================================================================
def make_name_val_pair(name,val,dbl_quoted_val=True):
    if dbl_quoted_val:
        val_to_write=quotedStr(val)
    else:
        val_to_write=val
    return dict(name=name,value=val_to_write)
#==================================================================================================

#==================================================================================================
def match_node_name_unique(nodes,name):
    matches = [ n for n in nodes if n["name"]==name ]
    Nmatch = len(matches)
    if Nmatch==1:
        return matches[0]
    else:
        raise RuntimeError("Expected exactly 1 match for node with name %s, but found %d" % (name,Nmatch))
#==================================================================================================

#==================================================================================================
def process(yml,config):
    new_yml = copy.deepcopy(yml)

    # Record the name of the last dag tasks before any other modifications
    orig_last_dag_task_name = (get_dag_tasks(yml))[-1]["name"]

    add_spec_settings(new_yml)
    add_top_level_meta(new_yml,config)
    remap_image_tags(new_yml,config["image_tag_map"])
    remove_nodes_from_templates(new_yml,["volumes"])

    # Find artifact, convert them to data transfer tasks, then record the new shared volume paths
    templates_with_data_transfers = convert_artifacts_to_data_transfer_tasks(new_yml)
    add_shared_vol_subpaths_for_transfers(config, templates_with_data_transfers)

    # Add template and task to create directories in the shared volume
    add_dir_struct_creator_template(new_yml,config["shared_vol"])
    add_dir_struct_creator_task(new_yml)

    # Add template for the data retriever
    add_data_retriever_template(new_yml, config["credentials"])
    # Add one retriever task for each dataset in the options file
    for task_opts in config["data_retriever"]["tasks"]:
        add_data_retriever_task(new_yml, task_opts)

    add_publisher(new_yml,config)

    add_common_container_opts(new_yml,config)
    reorder_container_keys(new_yml)
    add_shared_data_vol(new_yml,config["shared_vol"])

    # Rearrange dag tasks
    rearrange_dag(new_yml)

    # Set necessary dependencies for new dag tasks
    set_new_dag_dependencies(new_yml,orig_last_dag_task_name)

    return new_yml
#==================================================================================================

#==================================================================================================
def read_config_files(wf_dir):
    # Read main options file
    fpath = os.path.join(wf_dir,"dafni_options.yml")
    if os.path.isfile(fpath):
        config = load(fpath)
    else:
        exit("No dafni options yml at [%s]" % fpath)

    # Setup the image tag map
    local_repo="localhost"
    image_tag_map={}
    for img in config["images"]:
        local_tag = "%s/%s:%s" % (local_repo,img["local_name"],img["local_version"])
        dafni_tag = make_image_tag(img["dafni_name"],img["dafni_version"])
        image_tag_map[local_tag] = dafni_tag
    config["image_tag_map"] = image_tag_map

    # Read private settings (credentials etc.) from a separate file
    fpath = os.path.join(wf_dir,"../dafni_settings_private.yml")
    if os.path.isfile(fpath):
        private_settings = load(fpath)
    else:
        exit("No dafni options yml at [%s]" % fpath)
    # Add private settings to the config, handling nested dictionaries
    config = update_nested_dict(config,private_settings)

    return config
#==================================================================================================

#==================================================================================================
def rearrange_dag(yml):
    dag_template = get_dag_template(yml)
    dag_template["tasks"] = sorted(dag_template["tasks"],key=get_dag_sort_idx)
#==================================================================================================

#==================================================================================================
def remap_image_tags(yml,image_tag_map):
    for template in yml["spec"]["templates"]:
        if "container" in template:
            current_tag = template["container"]["image"]
            if current_tag in image_tag_map:
                template["container"]["image"] = image_tag_map[current_tag]
#==================================================================================================

#==================================================================================================
def remove_all_artifacts(yml):
    # Remove all artifacts from dag tasks
    dag_tasks = get_dag_tasks(yml)
    for task in dag_tasks:
        if "arguments" in task:
            discard = task["arguments"].pop("artifacts",{})

    # Remove all artifacts from templates
    for template in get_container_template_nodes(yml):
        for key in ["inputs","outputs"]:
            if key in template:
                discard = template[key].pop("artifacts",[])
                if not template[key]:
                    discard = template.pop(key,{})

#==================================================================================================

#==================================================================================================
def remove_nodes_from_templates(yml,node_names):
    for template in yml["spec"]["templates"]:
        for node_name in node_names:
            template.pop(node_name,None)
#==================================================================================================

#==================================================================================================
def reorder_container_keys(yml):
    # Reorder keys, purely to simplify comparisons with workflows generated by the DAFNI admins
    for template in get_container_template_nodes(yml):
        keys = ["image", "command"]
        keys_to_reorder = [k for k in sorted(list(template["container"].keys())) if k not in keys]
        keys.extend(keys_to_reorder)
        for k in keys:
            if k in template["container"]:
                template["container"][k] = template["container"].pop(k)
#==================================================================================================

#==================================================================================================
def resolve_template_path(raw_path,params):
    import re
    re_match = re.search('{{(.*)}}',raw_path)
    if re_match:
        tmp = re_match.group(1)
        param_name = (tmp.split('.'))[-1]
        param__name_val = match_node_name_unique(params,param_name)
        path = raw_path.replace('{{'+tmp+'}}',param__name_val["value"])
        return path
    else:
        return raw_path
#==================================================================================================

#==================================================================================================
def save(yml,fpath):
    with open(fpath,"w") as fh:
        yaml.round_trip_dump(yml,fh,width=1e9)
#==================================================================================================

#==================================================================================================
def set_new_dag_dependencies(yml,orig_last_dag_task_name):
    dag_tasks = get_dag_tasks(yml)

    # Any tasks that used to start the workflow (those without dependencies) should now wait until all data retrieval tasks have finished
    orig_first_tasks = [t for t in dag_tasks if "dependencies" not in t and t["name"] != DIR_CREATOR_TASK_NAME]
    retriever_dag_tasks = get_dag_tasks_match_name(yml,RETRIEVER_TASK_NAME_PREFIX,exact=False)
    for task in orig_first_tasks:
        task["dependencies"] = [quotedStr(t["name"]) for t in retriever_dag_tasks]

    # Make publisher task dependent on original last task
    publisher_dag_task = (get_dag_tasks_match_name(yml,PUBLISHER_TASK_NAME,count=1))[0]
    orig_last_dag_task = (get_dag_tasks_match_name(yml,orig_last_dag_task_name,count=1))[0]
    publisher_dag_task["dependencies"] = [ quotedStr(orig_last_dag_task["name"]) ]
#==================================================================================================

#==================================================================================================
def update_nested_dict(d,u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update_nested_dict(d.get(k, {}), v)
        else:
            d[k] = v
    return d
#==================================================================================================

#==================================================================================================
def main(wf_name,unroll_loop_props=None):
    # Set some locations
    wf_dir   = os.path.join(os.path.dirname(__file__),wf_name)
    local_wf_fpath = os.path.join(wf_dir,"argo-workflow.local.yml")
    dafni_wf_fpath  = os.path.join(wf_dir,"argo-workflow.dafni.yml")

    if not os.path.isfile(local_wf_fpath):
        exit("No local workflow file found at %s" % local_wf_fpath)

    # Read dafni options
    config = read_config_files(wf_dir)

    # Load the local workflow yml
    local_wf_yml = load(local_wf_fpath)

    # Unroll if 'unroll_loop_props' was set
    if unroll_loop_props is not None:
        local_wf_yml = load_unrolled(local_wf_yml,unroll_loop_props)

    # Add dafni options
    dafni_wf_yml  = process(local_wf_yml,config)

    # Save the new yml
    save(dafni_wf_yml,dafni_wf_fpath)
#==================================================================================================


if __name__ == "__main__":
    import sys
    if len(sys.argv)>=2:
        args = sys.argv[1:]
        wf_name = args.pop(0)
        kwargs={}
        for tag in ["-u","--unroll"]:
            try:
                idx = args.index(tag)
                kwargs["unroll_loop_props"] = args[idx+1]
                args.pop(idx+1)
                args.pop(idx)
            except (ValueError,) as e:
                pass
        
        if len(args):
            print("Ignored unrecognised args [%s]" % ",".join(args))

        main(wf_name,**kwargs)
    else:
        print("usage: python %s [workflow_subdirectory] <--unroll/-u first_iter,last_iter,iter_stride>" % sys.argv[0])