apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: "intermediate-credo-workflow"
  namespace: "argo"
spec:
  entrypoint: intermediate-credo-workflow
  arguments:
    parameters:
    - name: postgres-password
      value: postpass
    - name: postgres-user
      value: postgres
  templates:

  - name: intermediate-credo-workflow
    dag:
      tasks:

      - name: blazegraph
        template: blazegraph

      - name: postgis
        template: postgis
        arguments:
          parameters:
          - name: postgres-password
            value: "{{workflow.parameters.postgres-password}}"
          - name: postgres-user
            value: "{{workflow.parameters.postgres-user}}"

      - name: kg-initialiser
        template: kg-initialiser
        arguments:
          parameters:
          - name: blazegraph-host
            value: "{{tasks.blazegraph.ip}}"
          - name: ontop-config-output-dir
            value: "/data/outputs/ontop-config"
        dependencies: ["blazegraph"]

      - name: ontop
        template: ontop
        arguments:
          artifacts:
          - name: config
            from: "{{tasks.kg-initialiser.outputs.artifacts.ontop-config}}"
          parameters:
          - name: postgres-host
            value: "{{tasks.postgis.ip}}"
          - name: postgres-password
            value: "{{workflow.parameters.postgres-password}}"
          - name: postgres-user
            value: "{{workflow.parameters.postgres-user}}"
          - name: props-filename
            value: credo.properties
        dependencies: ["kg-initialiser", "postgis"]

      - name: pgsql-uploader
        template: pgsql-uploader
        arguments:
          parameters:
          - name: all-inputs-dir
            value: "/data/inputs/all"
          - name: postgres-host
            value: "{{tasks.postgis.ip}}"
          - name: postgres-password
            value: "{{workflow.parameters.postgres-password}}"
          - name: postgres-user
            value: "{{workflow.parameters.postgres-user}}"
          - name: tifs-outputs-dir
            value: "/data/outputs/tifs"
        dependencies: ["ontop"]


      - name: kg-data-extractor
        template: kg-data-extractor
        arguments:
          parameters:
          - name: blazegraph-host
            value: "{{tasks.blazegraph.ip}}"
          - name: blazegraph-password
            value: ""
          - name: blazegraph-user
            value: ""
          - name: ontop-host
            value: "{{tasks.ontop.ip}}"
          - name: output-dir
            value: "/data/outputs/kgdump"
          - name: postgres-host
            value: "{{tasks.postgis.ip}}"
          - name: postgres-password
            value: "{{workflow.parameters.postgres-password}}"
          - name: postgres-user
            value: "{{workflow.parameters.postgres-user}}"
          - name: timeseries-db
            value: "credo_timeseries"
        dependencies: ["pgsql-uploader"]


  - name: blazegraph
    container:
      image: localhost/blazegraph:1.0.0
      command: [ "catalina.sh", "run" ]
      readinessProbe:
        httpGet:
          path: /blazegraph
          port: 8080
        initialDelaySeconds: 10
        timeoutSeconds: 2
    daemon: true


  - name: kg-data-extractor
    container:
      image: localhost/kg-data-extractor:1.0.0
      command: [ "/app/entrypoint.sh" ]
      env:
        - name: KG_PASSWORD
          value: "{{inputs.parameters.blazegraph-password}}"
        - name: KG_URL
          value: "http://{{inputs.parameters.blazegraph-host}}:8080/blazegraph/namespace/kb/sparql"
        - name: KG_USER
          value: "{{inputs.parameters.blazegraph-user}}"
        - name: ONTOP_URL
          value: "http://{{inputs.parameters.ontop-host}}:8080/sparql"
        - name: OUTPUT_DIR
          value: "{{inputs.parameters.output-dir}}"
        - name: POSTGRES_PASSWORD
          value: "{{inputs.parameters.postgres-password}}"
        - name: POSTGRES_URL
          value: "jdbc:postgresql://{{inputs.parameters.postgres-host}}:5432/"
        - name: POSTGRES_USER
          value: "{{inputs.parameters.postgres-user}}"
        - name: TIMESERIES_DB
          value: "{{inputs.parameters.timeseries-db}}"
    inputs:
      parameters:
      - name: blazegraph-host
      - name: blazegraph-password
      - name: blazegraph-user
      - name: ontop-host
      - name: output-dir
      - name: postgres-host
      - name: postgres-password
      - name: postgres-user
      - name: timeseries-db
    outputs:
      artifacts:
      - name: kg-outputs
        path: "{{inputs.parameters.output-dir}}"


  - name: kg-initialiser
    container:
      image: localhost/kg-initialiser:1.0.0
      command: [ "/entrypoint.sh" ]
      env:
        - name: BLAZEGRAPH_HOST
          value: "{{inputs.parameters.blazegraph-host}}"
        - name: BLAZEGRAPH_PORT
          value: "8080"
        - name: INPUT_DIRS
          value: "/data/inputs/tb/tboxes /data/inputs/tb/tboxes/aw /data/inputs/tb/tboxes/bt /data/inputs/tb/tboxes/ukpn"
        - name: OBDA_INPUT_DIR
          value: "/data/inputs/obda/"
        - name: OBDA_OUTPUT_DIR
          value: "{{inputs.parameters.ontop-config-output-dir}}"
        - name: TBOX_OUTPUT_DIR
          value: "{{inputs.parameters.ontop-config-output-dir}}"
    inputs:
      parameters:
      - name: blazegraph-host
      - name: ontop-config-output-dir
    outputs:
      artifacts:
      - name: ontop-config
        path: "{{inputs.parameters.ontop-config-output-dir}}"


  - name: ontop
    container:
      image: localhost/ontop:1.0.0
      command: [ "/entrypoint.sh" ]
      env:
      - name: ONTOP_CORS_ALLOWED_ORIGINS
        value: "*"
      - name: ONTOP_DEV_MODE
        value: "False"
      - name: ONTOP_LAZY_INIT
        value: "True"
      - name: ONTOP_MAPPING_FILE
        value: /data/inputs/ontop-config/credo.obda
      - name: ONTOP_PROPERTIES_FILE
        value: "/data/inputs/props/{{inputs.parameters.props-filename}}"
      - name: ONTOP_PROPERTIES_FILE_TEMPLATE
        value: "/tmp/{{inputs.parameters.props-filename}}.template"
      - name: POSTGRES_HOST
        value: "{{inputs.parameters.postgres-host}}"
      - name: POSTGRES_PASSWORD
        value: "{{inputs.parameters.postgres-password}}"
      - name: POSTGRES_PORT
        value: "5432"
      - name: POSTGRES_USER
        value: "{{inputs.parameters.postgres-user}}"
      readinessProbe:
        httpGet:
          path: /
          port: 8080
        initialDelaySeconds: 10
        timeoutSeconds: 2
    daemon: true
    inputs:
      artifacts:
      - name: config
        path: /data/inputs/ontop-config
      parameters:
      - name: postgres-host
      - name: postgres-password
      - name: postgres-user
      - name: props-filename


  - name: pgsql-uploader
    container:
      image: localhost/pgsql-uploader:1.0.0
      command: [ "/bin/sh", "-c", "/entrypoint.sh" ]
      env:
        - name: CONFIG_DIR
          value: "{{inputs.parameters.all-inputs-dir}}/config_files"
        - name: DATA_DIR
          value: "{{inputs.parameters.all-inputs-dir}}"
        - name: GEOTIF_OUT_DIR
          value: "{{inputs.parameters.tifs-outputs-dir}}/"
        - name: g_CSV_ENABLED
          value: "1"
        - name: g_SHP_ENABLED
          value: "1"
        - name: g_TIF_ENABLED
          value: "1"
        - name: POSTGRES_HOST
          value: "{{inputs.parameters.postgres-host}}"
        - name: POSTGRES_PASSWORD
          value: "{{inputs.parameters.postgres-password}}"
        - name: POSTGRES_PORT
          value: "5432"
        - name: POSTGRES_USER
          value: "{{inputs.parameters.postgres-user}}"
    inputs:
      parameters:
      - name: all-inputs-dir
      - name: postgres-host
      - name: postgres-password
      - name: postgres-user
      - name: tifs-outputs-dir
    outputs:
      artifacts:
      - name: tif-outputs
        path: "{{inputs.parameters.tifs-outputs-dir}}/flood_data"


  - name: postgis
    container:
      image: localhost/postgis:1.0.0
      command: [ "docker-entrypoint.sh", "postgres" ]
      env:
      - name: POSTGIS_ENABLE_OUTDB_RASTERS
        value: "1"
      - name: POSTGIS_GDAL_ENABLED_DRIVERS
        value: "ENABLE_ALL"
      - name: POSTGRES_HOST_AUTH_METHOD
        value: "scram-sha-256"
      - name: POSTGRES_INITDB_ARGS
        value: "--auth-host=scram-sha-256"
      - name: POSTGRES_PASSWORD
        value: "{{inputs.parameters.postgres-password}}"
      - name: POSTGRES_USER
        value: "{{inputs.parameters.postgres-user}}"
    daemon: true
    inputs:
      parameters:
      - name: postgres-password
      - name: postgres-user
