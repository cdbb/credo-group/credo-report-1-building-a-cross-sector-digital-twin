#!/bin/bash

#--------------------------------------------------------------------------------------------------
# Helper function to generate each workflow and rename the file
gen_unrolled_workflow() {
  local wf_name=$1
  local timestep_props=$2
  local label=$3

  echo "Generating unrolled [$label] workflow with timestep properties [$timestep_props] "
  python ./unroll_loop.py "$wf_name" "$timestep_props"
  dest_file="./$wf_name/argo-workflow.$label.local.unrolled.yml"
  mv "./$wf_name/argo-workflow.local.unrolled.yml" "$dest_file"
  echo " Moved [$label] workflow file to [$dest_file]"
}
#--------------------------------------------------------------------------------------------------

#==================================================================================================
# Main

wf_name="full"

# Activate the venv, creating it first if necessary
venv_path=/tmp/dafni_venv
activate_script="$venv_path/bin/activate"
if [ -f "$activate_script" ]; then
    . "$activate_script"
else
    echo "Creating python venv at $venv_path"
    mkdir -p $venv_path && python3 -m venv $venv_path
    . "$activate_script"
    pip install -r requirements.txt
fi

# Generate each of the workflows in turn
gen_unrolled_workflow "$wf_name" "3600,18000,3600" "EA"
gen_unrolled_workflow "$wf_name" "3600,93600,3600" "hiPIMS_1h"
gen_unrolled_workflow "$wf_name" "3600,97200,3600" "hiPIMS_3h"
gen_unrolled_workflow "$wf_name" "3600,108000,3600" "hiPIMS_6h"
gen_unrolled_workflow "$wf_name" "3600,147600,3600" "hiPIMS_8h"
#==================================================================================================
