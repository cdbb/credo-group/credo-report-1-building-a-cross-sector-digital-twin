#!/bin/sh

if [ -z "$INPUT_DATASLOT_DIR" ]; then
    echo "ERROR: INPUT_DATASLOT_DIR is not set"
fi

if [ -z "$OUTPUT_DATASLOT_DIR" ]; then
    echo "ERROR: OUTPUT_DATASLOT_DIR is not set"
fi

if [ -e "$INPUT_DATASLOT_DIR" ]; then
    mkdir -p $OUTPUT_DATASLOT_DIR
    outfile="$OUTPUT_DATASLOT_DIR/input_contents.txt"
    find "$INPUT_DATASLOT_DIR" > "$outfile"
else
    echo "No file or directory found at input dataslot location ($INPUT_DATASLOT_DIR)"
fi
