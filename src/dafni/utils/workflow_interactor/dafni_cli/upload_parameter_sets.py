from dafni_cli.urls import WORKFLOWS_URL,PARAMETER_UPLOAD_URL
import requests
import json
import csv

def upload_parameter_sets(jwt:str, hipims_file, workflowID):
    auth_header = {"Authorization": jwt}

    # extract parameter set
    workflowGet = requests.get(
        WORKFLOWS_URL + workflowID,
        headers=auth_header
    )

    workflow = json.loads(workflowGet.text)
    for parameterSet in workflow['parameter_sets']:
        if (parameterSet['metadata']['name'] == 'testing'):
            parameter_json = parameterSet
            parameter_testing = parameterSet
            parameter_testing['workflowID'] = workflowID
            break

    # # manually upload
    # parameter_testing['metadata']['display_name'] = 'Parameter set for Ben'
    # parameter_testing['metadata']['name'] = 'testing-ben'
    # response = requests.post(
    #     PARAMETER_UPLOAD_URL,
    #     headers=auth_header,
    #     json=parameter_json,
    # )
    # print(response)

    csvreader = csv.DictReader(hipims_file)
    for row in csvreader:
        final_timestep = int(row['final timestep'])
        step_size = int(row['step size'])
        data_dafni_id = row['ID']

        # create timestep array
        timesteps_array = [step_size]
        while (timesteps_array[-1] < final_timestep):
            timesteps_array.append(timesteps_array[-1] + step_size)
        
        for step in parameter_json['spec']:
            # replace timestep array in the template
            if (step['step'] == 'timestep-loop'):
                step['parameters'][0]['values'] = timesteps_array
            
            # dataslot for flood data 
            elif (step['step'] == 'raster-transformer'):
                for dataslot in step['dataslots']:
                    if (dataslot['name'] == 'Flood data'):
                        dataslot['datasets'] = [data_dafni_id]

        # display title in front end
        parameter_json['metadata']['display_name'] = row['title']
        parameter_json['metadata']['name'] = row['code'].replace('_', '-').lower() # requirements of dafni
        description_text = 'Applies the digital twin to analyse the effects of a HiPIMS simulation of a ' + row['description'] + ' (' + row['code'] + ').'
        parameter_json['metadata']['description'] = description_text
        parameter_json['workflowID'] = workflowID

        response = requests.post(
            PARAMETER_UPLOAD_URL,
            headers=auth_header,
            json=parameter_json,
        )

        print(response)

    