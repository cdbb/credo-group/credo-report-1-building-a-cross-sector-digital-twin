# Overview
Script written to upload parameter sets to a workflow

# Installation
Install and activate python virtual environment (these instructions are for Windows environment)
```
python -m venv .venv
.venv\Scripts\activate
pip install -r requirements.txt
```

# Running the script
Instructions for running the script can be obtained from 
```
dafni_workflow_interactor.py --help
```

