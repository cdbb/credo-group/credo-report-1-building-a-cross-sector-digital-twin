#!/bin/sh

if [ -z "$INPUT_DATASLOT_DIR" ]; then
    echo "ERROR: INPUT_DATASLOT_DIR is not set"
fi

if [ -z "$OUTPUT_DATASLOT_DIR" ]; then
    echo "ERROR: OUTPUT_DATASLOT_DIR is not set"
fi

if [ -d "$INPUT_DATASLOT_DIR" ]; then
    mkdir -p "$OUTPUT_DATASLOT_DIR"
    parent_dir=$(dirname "$OUTPUT_DATASLOT_DIR")
    if [ -d "$parent_dir" ]; then
        cd "$parent_dir"
        unzip "$INPUT_DATASLOT_DIR/*.zip" -d $(basename $OUTPUT_DATASLOT_DIR)
    else
        unzip "$INPUT_DATASLOT_DIR/*.zip" -d "$OUTPUT_DATASLOT_DIR"
    fi
else
    echo "No zip file found at input dataslot location ($INPUT_DATASLOT_DIR)"
fi
