#!/bin/sh

podman load -i credo-vis.tar &&
podman load -i geoserver.tar &&
podman-compose up -d
