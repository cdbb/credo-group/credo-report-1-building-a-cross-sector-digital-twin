let queryString = window.location.search;
let urlParams = new URLSearchParams(queryString);

let winURL = window.location;
let fileName = urlParams.get("file");
let fullFileURL = winURL.protocol + "//" + winURL.host + winURL.pathname;
fullFileURL = fullFileURL.replace("/view", "");
fullFileURL += "/docs/" + fileName;
	
// Set content in header sections
var lineOne = `<b>File Name:</b> <span data-file='` + fullFileURL + `' onclick='openFile(this.dataset.file)' class='link'>` + fileName + `</span>`;
let lineTwo = "<b>Last Modified:</b> " + fetchHeader(fullFileURL, "Last-Modified");
let lineThree = "<b>File Size:</b> " + fetchHeader(fullFileURL, "Content-Length") + " bytes";
let header = lineOne + "<br/>" + lineTwo + "<br/>" + lineThree;
document.getElementById("name-container").innerHTML = header;

console.log("Attempting to get file at:" + fullFileURL);

// Show the content of the file
 var request = new XMLHttpRequest();
request.open('GET', fullFileURL, true);
request.send(null);
  request.onreadystatechange = function () {
	if (request.readyState === 4 && request.status === 200) {
		var type = request.getResponseHeader('Content-Type');
		console.log("Got file, type is " + type);
		
		if (type.indexOf("text") !== 1) {
			if(fullFileURL.endsWith("md")) {
				document.querySelector("#file-container").classList.remove("txt");
				document.querySelector("#file-container").innerHTML =  marked.parse(request.responseText);
				
			} else {
				document.querySelector("#file-container").classList.add("txt");
				document.querySelector("#file-container").textContent = request.responseText;
			}			
		}
	}
}
	

function fetchHeader(url, header) {
	try {
		var req=new XMLHttpRequest();
		req.open("HEAD", url, false);
		req.send(null);
		if(req.status == 200){
			return req.getResponseHeader(header);
		}
		else return false;
	} catch(er) {
		return er.message;
	}
}

function openFile(url) {
	myWindow = window.open(url);
	myWindow.focus();
}