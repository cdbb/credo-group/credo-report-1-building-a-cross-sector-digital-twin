<?php

	function console($data) {
		$output = $data;
		if (is_array($output))
			$output = implode(',', $output);

		echo "<script>console.log('" . $output . "' );</script>";
	}

	function getDirContents($dir, $depth, &$results = array()) {
		$files = scandir($dir);

		foreach ($files as $key => $value) {
			$path = realpath($dir . DIRECTORY_SEPARATOR . $value);
			$prefix = "<span style='margin-left: ".(25 * $depth).";'";
			
			if (!is_dir($path)) {
				$fileName = basename($path);
				$fileValue = str_replace("/var/www/html/docs/", "", $path);		
				console("File value is ".$fileValue);
				
				$entry = $prefix." data-file='".$fileValue."' onclick='openFile(this)' class='fileLink'>".$fileName."</span>";
				$results[] = $entry;
				
			} else if ($value != "." && $value != "..") {
				
				
				$dirName = basename($path);		
				$results[] = $prefix." class='dirLink'>".$dirName.":</span>";
				getDirContents($path, $depth + 1, $results);
			}
		}
		console("Listed available files.");
		return $results;
	}

	$allFiles = getDirContents("/var/www/html/docs", 0);
	return "<br/>".implode("", $allFiles);
?>