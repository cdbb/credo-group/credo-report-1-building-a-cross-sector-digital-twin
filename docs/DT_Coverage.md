<p align="left">
    <img src="./Credo_TitleBar.svg" alt="CReDo: The National Digital Twin Climate Resilience Demonstrator project" width="600"/>
</p>

# Data Coverage
The following paragraphs summarise the data coverage of the current digital twin.

## Water network
- Clean water, sewage and sludge sub-sites (IDs, names, locations, MPANs, phone numbers).
- Plant item part-hood hierarchy (which assets contain which other assets, including the asset descriptions).
- Clean and waste water connections between sub-sites (based on the core site connections specified by the raw data).

## Energy network
- Primary substations (IDs, names, locations, phone numbers).
- Secondary substations (IDs, names, locations).
- Power connections between primary and secondary substations (including fallback options).
- Power connections between secondary substations and water network sub-sites.
- Power connections between secondary substations and communications network sites (exchanges and fibre cabinets).

## Telecoms network
- Exchanges (IDs, names, locations, MPANs).
- Fibre cabinets (IDs, names, locations, MPANs).
- Mobile masts (IDs, names, locations).
- Connections between exchanges and landline telephones (not yet via street cabinets).
- Connections between exchanges and fibre cabinets.
- Phone connections to substations.
- Phone connections to water network sub-sites.

## Asset states
- Flood state, flood depth and maximum flood depth.
- Availability of mains power.
- Availability of landline telephone connectivity.
- Availability of mains water (only between water network sites).
- Availability of sewage connection (only between water network sites).

# Data not included
The following paragraphs summarise data that are not included in the current digital twin. Such data could be included in subsequent iterations.

## Water Network
- Distribution zones for water supply.
- Granularity of connectivity at the sub-site level and below.

## Energy Network
- Power connections to mobile masts.

## Telecoms Network
- Locations of legacy cabinets.
- Outgoing connections from fibre cabinets.
- Mobile signal coverage provided by mobile masts (raw data are available but not considered at the moment).
- Connections between telephone exchanges and mobile masts.
- MPANs of the mobile masts and therefore the connectivity to the electrical grid.

# Assumptions and simplifications
The following paragraphs summarise the assumptions and simplifications made in the treatment of the data in the current digital twin.

## Common
- All assets have a `maximum flood depth', which is the depth of water at which an asset fails. 
- The maximum flood depths use assume values that need to be replaced with real data.
- All connections specified as being directly between major assets, with intermediate assets such as pipe junctions, telegraph poles and electrical pylons ignored, even when data are available.

## Water network
- Ignored the presence of backup power from batteries and generators.
- The data grouped sub-sites geographically into core sites, which are named after the primary sub-site in the group. The raw data specified water and sewage connections for core sites, while the rest of the data (e.g. locations and type of asset) were provided for the sub-sites and the assets within them. To reconcile this, it was decided to simplify the digital twin by assigning connections to the sub-site with the same name as the core site. An undesirable consequence of this choice was that it left some sub-sites unconnected. The possibility of manually connecting the sub-sites was considered, but rejected both because of the desire for an automated solution and because of the time-consuming nature of the task. The options of assuming that all sub-sites have the same connections as their core site, or of explicitly adding new assets (with aggregated locations) to represent the core sites were also considered, but not chosen because they made the digital twin more complex without substantially improving it.

## Energy network
- Power supply connections mapped by matching MPANs from assets to MPANs supplied by substations.
- The connectivity between primary and secondary substations was generated using heuristics rather than known connections.

## Telecoms network
- Ignored the presence of backup power from batteries and generators.
- Ignored incoming connections to mobile masts due to lack of data.
- Ignored mobile coverage.
