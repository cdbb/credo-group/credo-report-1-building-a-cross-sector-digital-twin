<p align="left">
    <img src="./Credo_TitleBar.svg" alt="CReDo: The National Digital Twin Climate Resilience Demonstrator project" width="600"/>
</p>

**Increasing our climate resilience through connected digital twins**

CReDo is a climate change adaptation digital twin looking at the impact of flooding on infrastructure interdependencies across energy, water and telecoms networks:
- Anglian Water's water and sewerage assets,
- BT's communication assets and
- UKPN's power network assets.

The CReDo digital twin includes data describing asset types, locations, connectivity, state and part-hood relations. The digital twin uses knowledge of the connectivity to resolve the system-wide cascade of effects on the state of the assets caused by a failure in any of the networks.

The visualization displays assets from each network. Clicking on an asset displays detailed information about the asset and its state in a side panel, along with controls to facilitate the exploration of the connections to and from the asset.

# How it works
The CReDo digital twin uses a knowledge graph to represent data describing the assets from each network. The data include information about the type and location of each asset, the state of each asset and the inter-connections between assets. The knowledge graph expresses the data as a directed graph, where the nodes of the graph are concepts or their instances (data items) and the edges of the graph are links between related concepts or instances. This approach provides a convenient way to represent arbitrarily structured data.

## Ontologies

### Top-level ontology
The starting point for building a knowledge graph is a type of schema, known as an ontology, that defines the concepts and relations that can be represented in the knowledge graph.  The CReDo digital twin uses a hierarchical set of ontologies to represent the data for the different networks. At the highest level of the hierarchy is a top-level ontology that defines core concepts that apply throughout the entire digital twin. The top-level ontology is shown in a simplified form in Figure 1.

<p align="center">
    <img src="./Credo_TopTBOX.svg" alt="Top-level ontology (simplified)" width="600"/>
    <figcaption align = "center"><b>Figure 1: Top-level ontology (simplified).</b></figcaption>
</p>

### Domain ontologies
Domain-specific ontologies that inherit from the concepts and relations defined in the top-level ontology exist for each of the water, power and communication networks. The domain ontologies define the concepts that are specific to each type of network. The digital twin is able to be queried in terms of the top-level ontology, using the inheritance relations to retrieve data about individual assets from the domain ontologies. This allows the domain specific ontologies to evolve and change, to support the development of the digital twin, without disrupting its core business logic.


## Information cascade
The CReDo digital twin uses the knowledge graph to resolve the cascade of effects caused by the failure of asset(s). This is a key requirement for modelling how different failure scenarios affect the combined cross-sector network of assets.

Figure 2 shows an example of how this is achieved. The knowledge graph represents the connections between assets and the state of each asset using relations that inherit from the top-level ontology. The knowledge graph additionally represents the interdependencies between the states. In the case of Figure 2, for example, whether Asset B has power depends on both whether Asset B is flooded and whether Asset A is able to supply power. Structuring the data in this manner makes it straightforward to resolve the cascade of effects caused by the failure of assets within the network.

<p align="center">
    <img src="./Credo_Cascade_Effect.svg" alt="Information cascade through the knowledge graph (simplified)" width="600"/>
    <figcaption align = "center"><b>Figure 2: Information cascade through the knowledge graph (simplified).</b></figcaption>
</p>

## Workflow
The CReDo digital twin is implemented as a workflow on the Data & Analytics Facility for National Infrastructure (DAFNI). The sequence of the models in the workflow is illustrated in Figure 3.

<p align="center">
    <img src="./Credo_Workflow.svg" alt="Workflow of the CReDo digital twin (simplified)" width="450"/>
    <figcaption align = "center"><b>Figure 3: Workflow of the CReDo digital twin (simplified).</b></figcaption>
</p>

The workflow starts by loading asset and flood data from the DAFNI platform. The data is expressed as a knowledge graph using the domain ontologies. The design is intended to allow the data from each asset owner to be hosted as separate knowledge graphs that can be queried as needed from the digital twin. Various processes operate on the knowledge graph within a time-stepping loop.
- The flood depth at the location of each asset is added to the knowledge graph.
- The impact of the current flood conditions on individual assets is assessed using probabilistic individual asset failure models developed via a process of Expert Elicitation (EE).
- The operational states of affected assets are updated and the impact of the changes cascaded through the knowledge graph.
- The impact of the current flood conditions on the operation of assets within each network is assessed using system-wide impact models based on Operations Research (OR) simulations.
- The operational states of affected assets are updated and the impact of the changes cascaded through the knowledge graph.

The resulting knowledge graph resolves the time history of the flood and the impact on the state of each asset. After the final time step, data is extracted from the knowledge graph for visualisation.

# Future development
In the future, CReDo could inform decisions in operations and capital planning, and real-time response to extreme weather events caused by climate change.

The following are suggestions for how the CReDo digital twin could be developed to achieve this vision.
- Addition of live data streams, similar to the open data from the Environment Agency reporting river levels.
- Extension of the digital twin to use Ordnance Survey data to identify critical infrastructure such as hospitals and schools to improve the assessment of the criticality of assets.
- Addition of alternative ways to view the data, for example, to trace connections between assets or to identify assets with the highest criticality scores.
- Addition of more types of asset, for example, to include roads in the failure modelling.
- Adoption of a distributed architecture to facilitate the ability of data owners to share data without changing how they host it.
- Development of the digital twin to use grounded ontologies.

# The CReDo Team
<p align="center">
    <img src="./CReDo_Team.png" alt="CReDo project team" width="600"/>
</p>
